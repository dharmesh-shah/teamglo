//
//  MainTabBarVC.swift
//  TeamGlo
//
//  Created by datt on 22/01/21.
//  Copyright © 2021 zaptech. All rights reserved.
//

import UIKit

//enum MainTapBarIndex : Int {
//    case myTeam = 0
//    case create = 1
//    case support = 2
//
//    func titleName() -> String {
//        switch self {
//        case .myTeam:
//            return Title.myTeam
//        case .create:
//            return Title.create
//        case .support:
//            return Title.support
//        }
//    }
//}

class MainTabBarVC: UITabBarController, MainStoryboarded {
    
    let btnCenterBig = UIButton(type: .custom)
    var gifView = UIView()
    var selectedTab = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.setNavigationLogo()
        addNotificationProfile()
        self.navigationController?.removeBG()
//        navButtonWithImg(#imageLiteral(resourceName: "MyAccount"), selector: #selector(btnAccountAction))
//        navButtonWithImg(#imageLiteral(resourceName: "Home"), selector: #selector(btnHomeAction), isLeft: true)
        mainThread {
            self.setTab()
        }
        self.setSelectedCenterBtn()
    }
    func setTab() {
        view.backgroundColor = .clear
        tabBar.backgroundImage = UIImage(color: .clear)
        tabBar.shadowImage = UIImage()
        tabBar.backgroundColor = .clear
        
        var bottonFrame = tabBar.frame
        bottonFrame.size.height += 15//view.safeAreaInsets.bottom
        
        let tabbarBackgroundView = RoundShadowView(frame: bottonFrame)
        tabbarBackgroundView.backgroundColor = .clear
        tabbarBackgroundView.frame = bottonFrame
        view.addSubview(tabbarBackgroundView)
        view.bringSubviewToFront(tabBar)
        
        self.addCenterButton(withImage: #imageLiteral(resourceName: "GoButton"), highlightImage: #imageLiteral(resourceName: "GoButton"))
        selectedTab = selectedIndex
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        mainThread {
            self.setSelectedCenterBtn()
        }
    }
    
    func addCenterButton(withImage buttonImage : UIImage, highlightImage: UIImage) {
        
        let buttonSize : CGFloat = 68
        
        
        btnCenterBig.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin]
        btnCenterBig.frame = CGRect(x: 0.0, y: 0.0, width: buttonSize, height: buttonSize)
        btnCenterBig.translatesAutoresizingMaskIntoConstraints = false
        btnCenterBig.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        btnCenterBig.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        btnCenterBig.setBackgroundImage(buttonImage, for: .normal)
        btnCenterBig.setBackgroundImage(highlightImage, for: .highlighted)
        btnCenterBig.setBackgroundImage(highlightImage, for: .selected)
        
        let rectBoundTabbar = self.tabBar.bounds
        let xx = rectBoundTabbar.midX
        let yy = rectBoundTabbar.midY
        btnCenterBig.center = CGPoint(x: xx, y: yy)
        
        
        self.view.addSubview(btnCenterBig)
        self.view.bringSubviewToFront(btnCenterBig)
        
        tabBar.topAnchor.constraint(equalTo: btnCenterBig.topAnchor, constant: (buttonSize - (self.tabBar.bounds.height - self.tabBar.safeAreaInsets.bottom - 15))).isActive = true
        tabBar.centerXAnchor.constraint(equalTo: btnCenterBig.centerXAnchor).isActive = true
        
        btnCenterBig.addTarget(self, action: #selector(handleTouchTabbarCenter), for: .touchUpInside)
    }
    
    @objc func handleTouchTabbarCenter(sender : UIButton) {
        if let count = self.tabBar.items?.count
        {
            let i = floor(Double(count / 2))
            self.selectedViewController = self.viewControllers?[Int(i)]
            setSelectedCenterBtn()
        }
    }
    
    func setSelectedCenterBtn() {
        if let count = self.tabBar.items?.count
        {
            let i = floor(Double(count / 2))
            if selectedIndex == i.int {
                btnCenterBig.isSelected = true
                let objCreateTeamVC = CreateTeamVC.instantiate()
                self.presentPanModal(objCreateTeamVC)
                selectedIndex = selectedTab
            } else {
                btnCenterBig.isSelected = false
                selectedTab = selectedIndex
            }
            updateCenterBigButton()
        }
    }
    
    
    @objc func btnAccountAction() {
//        let objMyAccountVC = MyAccountVC.instantiate()
//        let nav = SwipeNavigationController(rootViewController: objMyAccountVC)
//        nav.removeBG()
//        present(nav, animated: true, completion: nil)
//        self.navigationController?.pushViewController(objMyAccountVC)
    }
    
    @objc func btnHomeAction() {
//        let transition = CATransition()
//        transition.duration = 0.5
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromLeft
//        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
//        let objDashoboardVC = DashoboardVC.instantiate()
//        let nav = SwipeNavigationController(rootViewController: objDashoboardVC)
//        nav.removeBG()
////        objDashoboardVC.modalPresentationStyle = .overFullScreen
//        present(nav, animated: true, completion: nil)
    }
    
    func updateCenterBigButton() {
        if gifView.isHidden != btnCenterBig.isSelected { return }
        gifView.isHidden = false
        gifView.alpha = btnCenterBig.isSelected ? 0 : 1
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) {
            self.gifView.alpha = self.btnCenterBig.isSelected ? 1 : 0
        } completion: { (s) in
            self.gifView.isHidden = !self.btnCenterBig.isSelected
        }
    }
}


class RoundShadowView: UIView {
    
    let containerView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func layoutView() {
        
        // set the shadow of the view's layer
        layer.backgroundColor = UIColor.clear.cgColor
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 8
        let rect = bounds.insetBy(dx: 0, dy: 0)
        layer.shadowPath = UIBezierPath(rect: rect).cgPath
        containerView.layer.cornerRadius = 15
        containerView.backgroundColor = .white
        
        addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        // pin the containerView to the edges to the view
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
