//
//  CandidateHistoryVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 16/09/21.
//

import UIKit

class CandidateHistoryVC: UIViewController ,AppSettingStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblTechnology: UILabel!
    @IBOutlet weak var lblCandidateName: UILabel!
    
    @IBOutlet weak var imgTechnology: UIImageView!
    @IBOutlet weak var collTechnologyType: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var tblStatusList: ContentSizedTableView!
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationLogoWithBack()
    }
}
//MARK: - PanModalPresentable Methods
extension CandidateHistoryVC: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }

     var panScrollable: UIScrollView? {
         return scrollView
     }
    var cornerRadius: CGFloat {
        return 40
    }
     var longFormHeight: PanModalHeight {
         return .contentHeight(scrollView.contentSize.height + 50)
     }
     var anchorModalToLongForm: Bool {
         return false
     }
    var showDragIndicator : Bool {
        return false
    }
}

//MARK: - UICollectionViewDelegate and UICollectionViewDataSource Methods
extension CandidateHistoryVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collTechnologyType.dequeueReusableCell(withReuseIdentifier: "TechnologyTypeCell", for: indexPath) as! TechnologyTypeCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 30)
    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension CandidateHistoryVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblStatusList.dequeueReusableCell(withIdentifier: "StatusTblCell", for: indexPath) as! StatusTblCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
}
