//
//  MyTeamVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 23/09/21.
//

import UIKit

class MyTeamVC: UIViewController,AppSettingStoryboarded {

    @IBOutlet weak var tblCandidateList: ContentSizedTableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationLogoWithBack()
    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension MyTeamVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCandidateList.dequeueReusableCell(withIdentifier: "MyTeamTblCell", for: indexPath) as! MyTeamTblCell
        cell.btnCancel.action { [weak self](btn) in
            let objCandidateHistoryVC = CandidateHistoryVC.instantiate()
            self?.presentPanModal(objCandidateHistoryVC)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
