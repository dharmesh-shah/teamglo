//
//  CancelTeamVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 16/09/21.
//

import UIKit

class CancelTeamVC: UIViewController ,AppSettingStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblTechnology: UILabel!
    @IBOutlet weak var lblCandidateName: UILabel!
    
    @IBOutlet weak var imgTechnology: UIImageView!
    @IBOutlet weak var txtSelectReason: UITextField!
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - UIButton Action
    @IBAction func btnCancelAction(_ sender: UIButton) {
    }
    @IBAction func btnConfirmAction(_ sender: UIButton) {
    }
}
//MARK: - PanModalPresentable Methods
extension CancelTeamVC: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }

     var panScrollable: UIScrollView? {
         return nil
     }
    var cornerRadius: CGFloat {
        return 40
    }
     var longFormHeight: PanModalHeight {
         guard let scrollView = panScrollable else { return .contentHeight(580) }
         // called once during presentation and stored
         scrollView.layoutIfNeeded()
         return .contentHeight(scrollView.contentSize.height)
     }
     var anchorModalToLongForm: Bool {
         return false
     }
    var showDragIndicator : Bool {
        return false
    }

}
