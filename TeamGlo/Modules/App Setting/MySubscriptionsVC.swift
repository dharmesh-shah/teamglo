//
//  MySubscriptionsVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 23/09/21.
//

import UIKit

class MySubscriptionsVC: UIViewController,AppSettingStoryboarded {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblHireDateWithMoney: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var tblCandidateList: ContentSizedTableView!
    @IBOutlet weak var lblNextChangeDate: UILabel!
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationLogoWithBack()
    }
    
    //MARK: - UIButton Actions
    @IBAction func btnViewPaymentHistoryAction(_ sender: UIButton) {
    }
    
    @IBAction func btnChangePaymentMethodAction(_ sender: UIButton) {
    }
    
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension MySubscriptionsVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCandidateList.dequeueReusableCell(withIdentifier: "MyTeamTblCell", for: indexPath) as! MyTeamTblCell
        cell.btnCancel.action { [weak self](btn) in
            let objCancelTeamVC = CancelTeamVC.instantiate()
            self?.presentPanModal(objCancelTeamVC)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
