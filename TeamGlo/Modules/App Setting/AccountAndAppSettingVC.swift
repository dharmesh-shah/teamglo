//
//  AccountAndAppSettingVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 23/09/21.
//

import UIKit

struct AppSetting {
    var image : UIImage
    var title : String
    var desc : String
}
class AccountAndAppSettingVC: UIViewController,AppSettingStoryboarded {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblAppSetting: UITableView!
    
    var arrAppSetting = [AppSetting(image: #imageLiteral(resourceName: "User"), title: "Profile", desc: "Hired on June 15,2021 4000 USD per month"),AppSetting(image: #imageLiteral(resourceName: "File"), title: "Subscription", desc: "Hired on June 15,2021 4000 USD per month"),AppSetting(image: #imageLiteral(resourceName: "NotificationBlack"), title: "Notification", desc: "Hired on June 15,2021 4000 USD per month"),AppSetting(image: #imageLiteral(resourceName: "Myteam"), title: "My Team", desc: "Hired on June 15,2021 4000 USD per month"),AppSetting(image: #imageLiteral(resourceName: "Logout"), title: "Logout", desc: "Hired on June 15,2021 4000 USD per month")]
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationLogoWithBack()
    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension AccountAndAppSettingVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAppSetting.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblAppSetting.dequeueReusableCell(withIdentifier: "AppSettingTblCell", for: indexPath) as! AppSettingTblCell
        cell.imgSetting.image = arrAppSetting[indexPath.row].image
        cell.lblTitle.text = arrAppSetting[indexPath.row].title
        cell.lblDesc.text = arrAppSetting[indexPath.row].desc
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch arrAppSetting[indexPath.row].title {
        case "Profile":
            self.view.makeToast(AlertMessage.inProgress)
        case "Subscription":
            let objMySubscriptionVC = MySubscriptionsVC.instantiate()
            self.navigationController?.pushViewController(objMySubscriptionVC)
        case "Notification":
            self.view.makeToast(AlertMessage.inProgress)
        case "My Team":
            let objMyTeamVC = MyTeamVC.instantiate()
            self.navigationController?.pushViewController(objMyTeamVC)
        case "Logout":
            self.view.makeToast(AlertMessage.inProgress)
        default:
            break
        }
    }
}
