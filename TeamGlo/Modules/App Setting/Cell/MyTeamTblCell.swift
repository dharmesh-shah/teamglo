//
//  MyTeamTblCell.swift
//  TeamGlo
//
//  Created by Dharmesh on 23/09/21.
//

import UIKit

class MyTeamTblCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblTechnology: UILabel!
    @IBOutlet weak var lblCandidateName: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var imgTechnology: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
