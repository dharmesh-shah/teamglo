//
//  StatusTblCell.swift
//  TeamGlo
//
//  Created by Dharmesh on 16/09/21.
//

import UIKit

class StatusTblCell: UITableViewCell {

    @IBOutlet weak var imgLine: UIImageView!
    @IBOutlet weak var lblStatusDate: UILabel!
    @IBOutlet weak var lblStatusTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
