//
//  SplashVC.swift
//  GymUser
//
//  Created by datt on 27/02/20.
//  Copyright © 2020 zaptech. All rights reserved.
//

import UIKit
import WebKit

class SplashVC: UIViewController , MainStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var webView: WKWebView!
    var objWelcomeVC : WelcomeVC!
    var navHomeMasterVC : UINavigationController!

    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = Bundle.main.url(forResource: "Splash", withExtension: "gif")!
        webView.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        webView.load(request)
        delayWithSeconds(5.5) {
            self.dismiss(animated: false) { [self] in
                if objWelcomeVC == nil {
                    objWelcomeVC = WelcomeVC.instantiate()
                }
                navHomeMasterVC = SwipeNavigationController(rootViewController: objWelcomeVC)
                navHomeMasterVC.modalPresentationStyle = .overFullScreen
                navHomeMasterVC.navigationBar.isHidden = true
                navHomeMasterVC.view.isHidden = true
                self.present(navHomeMasterVC, animated: false) {
                    self.navHomeMasterVC.view.isHidden = false
                }
//                Global.shared.objMainTabBarVC?.setHomeScreenFirst()
            }
        }
    }

    deinit {
        print("splash deinit")
    }
}
