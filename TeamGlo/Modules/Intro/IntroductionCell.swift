//
//  IntroductionCell.swift
//  TeamGlo
//
//  Created by Dharmesh on 03/09/21.
//

import UIKit

class IntroductionCell: UICollectionViewCell {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIntro: UIImageView!
}
