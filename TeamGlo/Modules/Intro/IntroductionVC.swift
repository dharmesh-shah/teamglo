//
//  IntroductionVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 03/09/21.
//

import UIKit

//MARK: - Introduction Data
struct Introduction {
    var title : String
    var desc : String
    var image : UIImage
}

class IntroductionVC: UIViewController,MainStoryboarded {
    
    //MARK: - IBOUtlets
    @IBOutlet weak var collIntroView: UICollectionView!

    //MARK: - Variable Declaration
    var arrIntroduction : [Introduction] = [ Introduction(title: "Let’s Build Faster Your Team", desc: "Teamglo is an exclusive network of high-performing and efficient software developers.", image: #imageLiteral(resourceName: "Intro")) , Introduction(title: "Let’s Build Faster Your Team", desc: "Teamglo is an exclusive network of high-performing and efficient software developers.", image: #imageLiteral(resourceName: "Intro")), Introduction(title: "Let’s Build Faster Your Team", desc: "Teamglo is an exclusive network of high-performing and efficient software developers.", image: #imageLiteral(resourceName: "Intro"))]

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
//MARK: - UICollectionView Delegate and  DataSource Methods
extension IntroductionVC : UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrIntroduction.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collIntroView.dequeueReusableCell(withReuseIdentifier: "IntroductionCell", for: indexPath) as! IntroductionCell
        cell.imgIntro.image = arrIntroduction[indexPath.row].image
        cell.lblTitle.text = arrIntroduction[indexPath.row].title
        cell.lblDesc.text = arrIntroduction[indexPath.row].desc
        cell.btnNext.action { [weak self](btn) in
            if indexPath.row == 2 {
                let objLoginMasterVC = LoginMasterVC.instantiate()
                self?.navigationController?.pushViewController(objLoginMasterVC)
                return
            }
            var index = indexPath
            index.row += 1
            self?.collIntroView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collIntroView.size.width, height: collIntroView.size.height)
    }

}
