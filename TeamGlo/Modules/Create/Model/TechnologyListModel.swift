//
//  TechnologyListModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 15/09/21.
//

import Foundation

struct TechnologyListModel: Codable {
    var status: String?
    var data: [TechnologyListData]?
    var message: String?
}

// MARK: - Datum
struct TechnologyListData: Codable {
    var photo: String?
    var id, companyID: Int?
    var name: String?
    var status: Bool?
    var createdAt, updatedAt: String?
    @NullCodable var deletedAt: String?

    enum CodingKeys: String, CodingKey {
        case photo, id
        case companyID = "company_id"
        case name, status, createdAt, updatedAt, deletedAt
    }
}
