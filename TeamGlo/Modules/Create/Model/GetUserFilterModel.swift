//
//  GetUserFilterModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 15/09/21.
//

import Foundation

struct GetUserFilterModel: Codable {
    var status: String?
    var data: [GetUserFilterData]?
    var message: String?
}

class GetUserFilterData: Codable {
    var noOfDev: Int?
    var technology: Int?
    var technologyName: String?
    var technologyImage: String?
    var experience: String?
    var totalResource: Int?
    var id: Int?

    enum CodingKeys: String, CodingKey {
        case experience
        case noOfDev = "no_of_dev"
        case technology
        case technologyName = "technology_name"
        case technologyImage = "technology_image"
        case totalResource = "total_resource"
        case id
    }
}
