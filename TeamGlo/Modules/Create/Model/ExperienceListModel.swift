//
//  ExperienceListModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 17/09/21.
//

import Foundation

struct ExperienceListModel: Codable {
    var status: String?
    var data: [ExperienceListData]?
    var message: String?
}

// MARK: - Datum
struct ExperienceListData: Codable {
    var id: Int?
    var label, value, createdAt, updatedAt: String?
    @NullCodable var deletedAt: String?
}
