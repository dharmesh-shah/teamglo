//
//  CreateTeamVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 07/09/21.
//

import UIKit

class CreateTeamVC: UIViewController, CreateStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var viewCreateTeam: UIView!
    @IBOutlet weak var viewSingleLine: UIView!
    @IBOutlet weak var viewChooseTechnology: UIView!
    @IBOutlet weak var viewChooseExperience: UIView!
    @IBOutlet weak var viewChooseNos: UIView!
    
    @IBOutlet weak var txtSelectNos: UITextField!
    @IBOutlet weak var txtSelectExperience: UITextField!
    @IBOutlet weak var txtSelectTechnology: UITextField!
    @IBOutlet weak var collTechnology: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnCreateTeam: UIButton!
    
    
    //MARK: - Variable Declaration
    var viewModel = CreateTeamViewModel()
    
    var pickerTechnology = UIPickerView()
    var pickerExperience = UIPickerView()
    var pickerNos = UIPickerView()

    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
//        collTechnology.isHidden = true
//        viewSingleLine.isHidden = true
        setInitialViews()
//        createToolbar()
        getTechnologyList()
        if Global.shared.arrUserFilter.value.count == 0 {
            collTechnology.isHidden = true
            viewSingleLine.isHidden = true
        } else {
            collTechnology.isHidden = false
            viewSingleLine.isHidden = false
        }
        Global.shared.arrUserFilter.asDriver().drive (onNext: { [weak self] (arrUserFilter) in

            self?.collTechnology.reloadData()
        }).disposed(by: rx.disposeBag)
        
    }
    
    //MARK: - Setup UI
    func setInitialViews() {
        
        pickerTechnology.delegate = self
        pickerExperience.delegate = self
        pickerNos.delegate = self
        pickerTechnology.dataSource = self
        pickerExperience.dataSource = self
        pickerNos.dataSource = self

        txtSelectTechnology.delegate = self
        txtSelectExperience.delegate = self
        txtSelectNos.delegate = self

        txtSelectTechnology.inputView = pickerTechnology
        txtSelectExperience.inputView = pickerExperience
        txtSelectNos.inputView = pickerNos
        
    }
    func createToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()

        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissKeyboard))
        toolBar.setItems([spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        txtSelectTechnology.inputAccessoryView = toolBar
        txtSelectExperience.inputAccessoryView = toolBar
        txtSelectNos.inputAccessoryView = toolBar

    }
    @objc func dismissKeyboard(_sender : UIPickerView) {
        if pickerTechnology.tag == 100 {
            let row = pickerTechnology.selectedRow(inComponent: 0)
            pickerTechnology.selectRow(row, inComponent: 0, animated: false)
            txtSelectTechnology.text = viewModel.arrTechnology[row].name
        } else if pickerExperience.tag == 101 {
            let row1 = pickerExperience.selectedRow(inComponent: 0)
            pickerExperience.selectRow(row1, inComponent: 0, animated: false)
            txtSelectExperience.text = viewModel.arrExperienceList[row1].label
        } else {
            let row2 = pickerNos.selectedRow(inComponent: 0)
            pickerNos.selectRow(row2, inComponent: 0, animated: false)
            txtSelectNos.text = "\(viewModel.arrNos[row2])"
        }
        view.endEditing(true)
    }
    //MARK: - UIButton Action
    @IBAction func btnCreateTeamAction(_ sender: UIButton) {
        if txtSelectTechnology.isEmpty {
            self.view.makeToast(AlertMessage.selectTechnology)
        } else if txtSelectExperience.isEmpty {
            self.view.makeToast(AlertMessage.selectExperience)
        } else if txtSelectNos.isEmpty {
            self.view.makeToast(AlertMessage.selectNoOfPosition)
        } else {
            getTechnologyWithResource(btnCreateTeam)
        }
    }
    
    //MARK: - API Call
    func getTechnologyList() {
        viewModel.getTechnologyList() { [weak self] (error) in
            if let error = error {
                self?.view.makeToast(error)
            }
            self?.pickerTechnology.reloadAllComponents()
            self?.pickerNos.reloadAllComponents()
            self?.getExperienceList()
        }
    }
    func getExperienceList() {
        viewModel.getExperienceList() { [weak self] (error) in
            if let error = error {
                self?.view.makeToast(error)
            }
            self?.pickerExperience.reloadAllComponents()
//            self?.getTechnologyWithResource(self?.collTechnology)
        }
    }
    func getTechnologyWithResource(_ loaderView : UIView? = nil) {
        var param = [String:Any]()
        param[WebServiceParameter.experience] = viewModel.strExperience
        param[WebServiceParameter.technology] = viewModel.technologyId
        param[WebServiceParameter.noOfDev] = Int(txtSelectNos.text ?? "")
        
        viewModel.createFilterForTeam(loaderView, param) { [weak self](error,arrUserResource)  in
            if let error = error {
                self?.view.makeToast(error)
                return
            }
            self?.txtSelectTechnology.text = ""
            self?.txtSelectExperience.text = ""
            self?.txtSelectNos.text = ""
            self?.dismiss(animated: true)
        }
    }
}
//MARK: - PanModalPresentable Methods
extension CreateTeamVC: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }

     var panScrollable: UIScrollView? {
         return scrollView
     }
     var shortFormHeight: PanModalHeight {
        if collTechnology.isHidden {
            return .contentHeight(520)
        } else {
            return .maxHeight
        }
     }
    var cornerRadius: CGFloat {
        return 40
    }
     var anchorModalToLongForm: Bool {
         return false
     }
    var showDragIndicator : Bool {
        return false
    }
}
//MARK: - UICollectionViewDelegate and UICollectionViewDataSource Methods
extension CreateTeamVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Global.shared.arrUserFilter.value.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collTechnology.dequeueReusableCell(withReuseIdentifier: "TechnologyCell", for: indexPath) as! TechnologyCell
        let objTechnology = Global.shared.arrUserFilter.value[indexPath.row]
        cell.lblExpirienceWithNos.text = "\(objTechnology.experience ?? "")"
        cell.lblTechnology.text = objTechnology.technologyName
        cell.imgTechnology.setImgFullUrlWithNuke(objTechnology.technologyImage ?? "")
        cell.lblNoOfRequirements.text = "\(objTechnology.totalResource ?? 0)"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collTechnology.size.width - 140, height: 165)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objEditTeamVC = EditTeamVC.instantiate()
        objEditTeamVC.viewModel.objUserFilter = Global.shared.arrUserFilter.value[indexPath.row]
        self.presentPanModal(objEditTeamVC)
    }
}
extension CreateTeamVC : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerTechnology {
            return viewModel.arrTechnology.count
        } else if pickerView == pickerExperience {
            return viewModel.arrExperienceList.count
        } else {
            return viewModel.arrNos.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerTechnology {
            return viewModel.arrTechnology[row].name
        } else if pickerView == pickerExperience {
            return viewModel.arrExperienceList[row].label
        } else {
            return "\(viewModel.arrNos[row])"
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerTechnology {
            txtSelectTechnology.text = viewModel.arrTechnology[row].name
            viewModel.technologyId = viewModel.arrTechnology[row].id ?? 0
        } else if pickerView == pickerExperience {
            txtSelectExperience.text = viewModel.arrExperienceList[row].label
            viewModel.strExperience = viewModel.arrExperienceList[row].value ?? ""
        } else {
            txtSelectNos.text = "\(viewModel.arrNos[row])"
        }

    }
}
extension CreateTeamVC : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtSelectTechnology {
            let row = pickerTechnology.selectedRow(inComponent: 0)
            pickerTechnology.selectRow(row, inComponent: 0, animated: false)
            txtSelectTechnology.text = viewModel.arrTechnology[row].name
        } else if textField == txtSelectExperience {
            let row1 = pickerExperience.selectedRow(inComponent: 0)
            pickerExperience.selectRow(row1, inComponent: 0, animated: false)
            txtSelectExperience.text = viewModel.arrExperienceList[row1].label
        } else {
            let row2 = pickerNos.selectedRow(inComponent: 0)
            pickerNos.selectRow(row2, inComponent: 0, animated: false)
            txtSelectNos.text = "\(viewModel.arrNos[row2])"
        }
    }
}
