//
//  EditTeamVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 07/09/21.
//

import UIKit

class EditTeamVC: UIViewController, CreateStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var txtSelectNos: UITextField!
    @IBOutlet weak var txtSelectExperience: UITextField!
    @IBOutlet weak var txtSelectTechnology: UITextField!
    @IBOutlet weak var btnEditTeam: UIButton!

    //MARK: - Variable Declaration
    var viewModel = EditTeamViewModel()
    var myMatchesVM = MyMatchesViewModel()
    var pickerTechnology = UIPickerView()
    var pickerExperience = UIPickerView()
    var pickerNos = UIPickerView()

    //MARK: - View Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialViews()
        createToolbar()
        getTechnologyList()
    }
    //MARK: - Setup UI
    func setInitialViews() {
        
        pickerTechnology.delegate = self
        pickerExperience.delegate = self
        pickerNos.delegate = self
        pickerTechnology.dataSource = self
        pickerExperience.dataSource = self
        pickerNos.dataSource = self

        txtSelectTechnology.delegate = self
        txtSelectExperience.delegate = self
        txtSelectNos.delegate = self

        txtSelectTechnology.inputView = pickerTechnology
        txtSelectExperience.inputView = pickerExperience
        txtSelectNos.inputView = pickerNos
        
        viewModel.strExperience = viewModel.objUserFilter?.experience ?? ""
        viewModel.technologyId = viewModel.objUserFilter?.technology ?? 0
        txtSelectTechnology.text = viewModel.objUserFilter?.technologyName
        txtSelectNos.text = "\(viewModel.objUserFilter?.noOfDev ?? 0)"

    }
    func createToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()

        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissKeyboard))
        toolBar.setItems([spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        txtSelectTechnology.inputAccessoryView = toolBar
        txtSelectExperience.inputAccessoryView = toolBar
        txtSelectNos.inputAccessoryView = toolBar
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    //MARK: - UIButton Action
    @IBAction func btnEditTeamAction(_ sender: UIButton) {
        if txtSelectTechnology.isEmpty {
            self.view.makeToast(AlertMessage.selectTechnology)
        } else if txtSelectExperience.isEmpty {
            self.view.makeToast(AlertMessage.selectExperience)
        } else if txtSelectNos.isEmpty {
            self.view.makeToast(AlertMessage.selectNoOfPosition)
        } else {
            searchResources()
        }
    }
    //MARK: - API Call
    func getTechnologyList() {
        viewModel.getTechnologyList() { [weak self] (error) in
            if let error = error {
                self?.view.makeToast(error)
            }
            self?.pickerTechnology.reloadAllComponents()
            self?.pickerNos.reloadAllComponents()
            if let index = self?.viewModel.arrTechnology.firstIndex(where: { $0.name == self?.viewModel.objUserFilter?.technologyName ?? ""}) {
                self?.pickerTechnology.selectRow(index, inComponent: 0, animated: false)
            }
            if let index = self?.viewModel.arrNos.firstIndex(of: self?.viewModel.objUserFilter?.noOfDev ?? 0) {
                self?.pickerNos.selectRow(index, inComponent: 0, animated: false)
            }
            self?.getExperienceList()
        }
    }
    func getExperienceList() {
        viewModel.getExperienceList() { [weak self] (error) in
            if let error = error {
                self?.view.makeToast(error)
            }
            self?.txtSelectExperience.text = self?.viewModel.arrExperienceList.first(where: { (objExperienceListData) -> Bool in
                return objExperienceListData.value == self?.viewModel.objUserFilter?.experience
            })?.label
            self?.pickerExperience.reloadAllComponents()
            if let index = self?.viewModel.arrExperienceList.firstIndex(where: { $0.value == self?.viewModel.objUserFilter?.experience ?? ""}) {
                self?.pickerExperience.selectRow(index, inComponent: 0, animated: false)
            }
        }
    }
    func searchResources() {
        var param = [String:Any]()

        param[WebServiceParameter.experience] = viewModel.strExperience
        param[WebServiceParameter.technology] = viewModel.technologyId
        param[WebServiceParameter.noOfDev] = Int(txtSelectNos.text ?? "")
        
        viewModel.createFilterForTeam(btnEditTeam, param , filterId: viewModel.objUserFilter?.id) { [weak self](error,arrUserFilter) in
            if let error = error {
                self?.view.makeToast(error)
                return
            }
            self?.txtSelectTechnology.text = ""
            self?.txtSelectExperience.text = ""
            self?.txtSelectNos.text = ""
//            if let objCreateTeamVC = self?.presentingViewController as? CreateTeamVC, let arrUserFilter = arrUserFilter {
//                objCreateTeamVC.viewModel.arrUserFilter.accept(arrUserFilter)
//            }
            self?.dismiss(animated: true)
        }
    }


}
//MARK: - PanModalPresentable Methods
extension EditTeamVC: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }

     var panScrollable: UIScrollView? {
         return nil
     }
     var shortFormHeight: PanModalHeight {
        return .contentHeight(520)
     }
    var cornerRadius: CGFloat {
        return 40
    }
     var anchorModalToLongForm: Bool {
         return false
     }
    var showDragIndicator : Bool {
        return false
    }

}
//MARK: - UIPickerViewDelegate and UIPickerViewDataSource Method
extension EditTeamVC : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerTechnology {
            return viewModel.arrTechnology.count
        } else if pickerView == pickerExperience {
            return viewModel.arrExperienceList.count
        } else {
            return viewModel.arrNos.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerTechnology {
            return viewModel.arrTechnology[row].name
        } else if pickerView == pickerExperience {
            return viewModel.arrExperienceList[row].label
        } else {
            return "\(viewModel.arrNos[row])"
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerTechnology {
            txtSelectTechnology.text = viewModel.arrTechnology[row].name
            viewModel.technologyId = viewModel.arrTechnology[row].id ?? 0
        } else if pickerView == pickerExperience {
            txtSelectExperience.text = viewModel.arrExperienceList[row].label
            viewModel.strExperience = viewModel.arrExperienceList[row].value ?? ""
        } else {
            txtSelectNos.text = "\(viewModel.arrNos[row])"
        }

    }
}
extension EditTeamVC : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtSelectTechnology {
            let row = pickerTechnology.selectedRow(inComponent: 0)
            pickerTechnology.selectRow(row, inComponent: 0, animated: false)
            txtSelectTechnology.text = viewModel.arrTechnology[row].name
            viewModel.technologyId = viewModel.arrTechnology[row].id ?? 0
        } else if textField == txtSelectExperience {
            let row1 = pickerExperience.selectedRow(inComponent: 0)
            pickerExperience.selectRow(row1, inComponent: 0, animated: false)
            txtSelectExperience.text = viewModel.arrExperienceList[row1].label
            viewModel.strExperience = viewModel.arrExperienceList[row1].value ?? ""
        } else {
            let row2 = pickerNos.selectedRow(inComponent: 0)
            pickerNos.selectRow(row2, inComponent: 0, animated: false)
            txtSelectNos.text = "\(viewModel.arrNos[row2])"
        }
    }
}
