//
//  CreateTeamIntroVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 07/09/21.
//

import UIKit

class CreateTeamIntroVC: UIViewController, CreateStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var vCreateTeam: UIView!
    @IBOutlet weak var vInterview: UIView!
    @IBOutlet weak var vPayAndHire: UIView!

    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        vCreateTeam.addTapGestureRecognizer { 
            let objCreateTeamVC = CreateTeamVC.instantiate()
            self.presentPanModal(objCreateTeamVC)
        }
    }
}
