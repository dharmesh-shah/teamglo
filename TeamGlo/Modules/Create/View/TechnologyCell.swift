//
//  TechnologyCell.swift
//  TeamGlo
//
//  Created by Dharmesh on 07/09/21.
//

import UIKit

class TechnologyCell: UICollectionViewCell {
    
    @IBOutlet weak var lblNoOfRequirements: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblExpirienceWithNos: UILabel!
    @IBOutlet weak var lblTechnology: UILabel!
    @IBOutlet weak var imgTechnology: UIImageView!
}
