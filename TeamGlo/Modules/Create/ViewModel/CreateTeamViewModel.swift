//
//  File.swift
//  TeamGlo
//
//  Created by Dharmesh on 15/09/21.
//

import Foundation

class CreateTeamViewModel {
    //MARK: - Variable Declaration
    var arrTechnology : [TechnologyListData] = []
    var arrExperienceList : [ExperienceListData] = []
    var arrNos = [1,2,3,4,5,6,7,8,9,10]
    var technologyId = Int()
    var strExperience = String()
    var arrUserResource = UserDefaults.standard.retrieve(object: [GetUserFilterData].self, fromKey: UserDefaultKey.savedFilters)
}
extension CreateTeamViewModel {
    func getTechnologyList(completion: @escaping (_ error: String?) -> ()) {
        
        ApiCall().post(apiUrl: WebServiceURL.technologyList, params: [:], model: TechnologyListModel.self,isLoader: false) { result in
            switch result {
            
            case .success(let response):
                print(response)
                self.arrTechnology = response.data ?? []
                completion(nil)
            case .failure(let failure):
                completion(failure.message)
            case .error(let error):
                completion(error.debugDescription)
            }
        }
    }
    func getExperienceList(completion: @escaping (_ error: String?) -> ()) {
        
        ApiCall().get(apiUrl: WebServiceURL.experienceList, model: ExperienceListModel.self,isLoader: false) { result in
            switch result {
            
            case .success(let response):
                print(response)
                self.arrExperienceList = response.data ?? []
            completion(nil)
            case .failure(let failure):
                completion(failure.message)
            case .error(let error):
                completion(error.debugDescription)
            }
        }
    }
    func getUserFilter(_ loaderInView: UIView? = nil,_ completion: @escaping (_ error: String?,_ arrUserFilter: [GetUserFilterData]?) -> ()) {

        ApiCall().get(apiUrl: WebServiceURL.userGetFilters, model: GetUserFilterModel.self,loaderInView: loaderInView) { result in
            switch result {

            case .success(let response):
                print(response)
                Global.shared.arrUserFilter.accept(response.data ?? [])
            completion(nil,response.data)
            case .failure(let failure):
                completion(failure.message,nil)
            case .error(let error):
                completion(error.debugDescription,nil)
            }
        }
    }
    func createFilterForTeam(_ loaderInView: UIView? = nil, _ filterDic : [String:Any], filterId: Int? = nil,completion: @escaping (_ error: String?,_ arrUserFilter: [GetUserFilterData]?) -> ()) {
        
        var params = [String:Any]()
        params[WebServiceParameter.filters] = filterDic
        params[WebServiceParameter.filterId] = filterId

        ApiCall().post(apiUrl: WebServiceURL.searchResources, params: params, model: MyMatchesModel.self, loaderInView: loaderInView) { result in
            switch result {
            
            case .success(let response):
                print(response)
                Global.shared.arrMyMatches.accept(response.data?.resources ?? [])
                Global.shared.arrUserFilter.accept(response.data?.savedFilters ?? [])
                completion(nil, response.data?.savedFilters)
            case .failure(let failure):
                completion(failure.message, nil)
            case .error(let error):
                completion(error.debugDescription, nil)
            }
        }
    }


}
