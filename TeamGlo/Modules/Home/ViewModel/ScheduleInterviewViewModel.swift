//
//  ScheduleInterviewViewModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 14/09/21.
//

import UIKit

class ScheduleInterviewViewModel {
    var arrTimeZone = TimeZone.knownTimeZoneIdentifiers
}
extension ScheduleInterviewViewModel {
    
    func scheduleInterView(_ loaderInView: UIView,arrResourceIds : [Int],strDateTime : String,strTimeZone: String,completion: @escaping (_ error: String?) -> ()) {
        
        var params = [String:Any]()
        
        params[WebServiceParameter.resourceId] = arrResourceIds
        params[WebServiceParameter.scheduledDateTime] = strDateTime
        params[WebServiceParameter.timezone] = strTimeZone

        ApiCall().post(apiUrl: WebServiceURL.scheduleInterview, params: params, model: GeneralResponseModel.self, loaderInView: loaderInView) { result in
            switch result {
            
            case .success(let response):
                print(response)
                completion(nil)
            case .failure(let failure):
                completion(failure.message)
            case .error(let error):
                completion(error.debugDescription)
            }
        }
    }
}
