//
//  ReplaceCandidateViewModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 21/09/21.
//

import Foundation

class ReplaceCandidateViewModel {
    var arrMyMatches : [MyMatchesData] = []
    var arrUserFilter : [GetUserFilterData] = []
}
extension ReplaceCandidateViewModel {
    
    func replaceResourceData(_ loaderInView: UIView, _ filterDic : [[String:Any]],completion: @escaping (_ error: String?) -> ()) {
        
        var params = [String:Any]()
        params[WebServiceParameter.filters] = filterDic
        ApiCall().post(apiUrl: WebServiceURL.replaceResource, params: params, model: MyMatchesModel.self, loaderInView: loaderInView) { result in
            switch result {
            
            case .success(let response):
                print(response)
                self.arrMyMatches = response.data?.resources ?? []
                completion(nil)
            case .failure(let failure):
                completion(failure.message)
            case .error(let error):
                completion(error.debugDescription)
            }
        }
    }
}
