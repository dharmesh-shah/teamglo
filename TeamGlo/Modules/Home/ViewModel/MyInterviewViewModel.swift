//
//  MyInterviewViewModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 20/09/21.
//

import Foundation

class MyInterviewViewModel {
    var arrMyInterviewsData : [MyInterviewsData] = []
}
extension MyInterviewViewModel {
    
    func getMyInterviewsData(_ loaderInView: UIView,_ completion: @escaping (_ error: String?) -> ()) {
        
        ApiCall().get(apiUrl: WebServiceURL.meetings, model: MyInterviewsModel.self,loaderInView: loaderInView) { result in
            switch result {
            
            case .success(let response):
                print(response)
                self.arrMyInterviewsData = response.data ?? []
                completion(nil)
            case .failure(let failure):
                completion(failure.message)
            case .error(let error):
                completion(error.debugDescription)
            }
        }
    }
}
