//
//  CandidateTblCell.swift
//  TeamGlo
//
//  Created by Dharmesh on 09/09/21.
//

import UIKit

class CandidateTblCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblTechnology: UILabel!
    @IBOutlet weak var lblCandidateName: UILabel!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnReplace: UIButton!
    @IBOutlet weak var imgTechnology: UIImageView!
    @IBOutlet weak var collTechnologyType: UICollectionView!
    
    //MARK: - Variable Declaration
    var arrTechnology : [ResourceTechnology] = []

    //MARK: - Cell Init Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        collTechnologyType.delegate = self
        collTechnologyType.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
//MARK: - UICollectionViewDelegate and UICollectionViewDataSource Methods
extension CandidateTblCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTechnology.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collTechnologyType.dequeueReusableCell(withReuseIdentifier: "TechnologyTypeCell", for: indexPath) as! TechnologyTypeCell
        cell.lblTechnologyType.text = arrTechnology[indexPath.row].technology?.name
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 30)
    }
}
