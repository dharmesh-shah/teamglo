//
//  InterviewTblCell.swift
//  TeamGlo
//
//  Created by Dharmesh on 10/09/21.
//

import UIKit

class InterviewTblCell: UITableViewCell {

    @IBOutlet weak var lblInterviewDateTime: UILabel!
    @IBOutlet weak var nslcTbleCandidateHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCandidateList: ContentSizedTableView!
    
    var count : Int!
    var tblheight : CGFloat = 0.0
    var viewModel = MyMatchesViewModel()
    override func awakeFromNib() {
        super.awakeFromNib()
        tblCandidateList.delegate = self
        tblCandidateList.dataSource = self
    
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
//    //MARK: - Reload UITableView Method
    func relaodCandidateList() {
        
        tblCandidateList.invalidateIntrinsicContentSize()
        tblCandidateList.reloadData {
            if self.tblheight == 0 {
                self.tableView?.beginUpdates()
                self.tableView?.endUpdates()
                self.tblheight = self.tblCandidateList.contentSize.height
            }
//            self.nslcTbleCandidateHeight.constant = self.intrinsicTableViewContentSize.height
        }
    }
//    var intrinsicTableViewContentSize: CGSize {
//        return self.tblCandidateList.contentSize
//    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension InterviewTblCell : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3//viewModel.arrMyMatches.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCandidateList.dequeueReusableCell(withIdentifier: "CandidateTblCell", for: indexPath) as! CandidateTblCell
//        if let imgLogoPath = viewModel.arrMyMatches[indexPath.row].photo {
//                cell.imgTechnology.setImgFullUrlWithNuke(imgLogoPath)
//        }
//        cell.lblCandidateName.text = viewModel.arrMyMatches[indexPath.row].name
//        cell.lblTechnology.text = viewModel.arrMyMatches[indexPath.row].intro
//        cell.lblExperience.text = "Experience : \(viewModel.arrMyMatches[indexPath.row].experience ?? 0) Years"
//        cell.lblAmount.text = "$\(viewModel.arrMyMatches[indexPath.row].price ?? 0)/-Year"
//        cell.arrTechnology = viewModel.arrMyMatches[indexPath.row].resourceTechnologies ?? []
        cell.collTechnologyType.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
}
