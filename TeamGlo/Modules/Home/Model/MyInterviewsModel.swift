//
//  MyInterviewsModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 20/09/21.
//

import Foundation

struct MyInterviewsModel: Codable {
    var status: String?
    var data: [MyInterviewsData]?
    var message: String?
}

// MARK: - Datum
struct MyInterviewsData: Codable {
    var id, userID: Int?
    var resourceID: [Int]?
    var scheduledDateTime, timezone: String?
    var status: Int?
    var createdAt, updatedAt: String?
    @NullCodable var deletedAt: String?
    var resourceData: [MyMatchesData]?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case resourceID = "resource_id"
        case scheduledDateTime = "scheduled_date_time"
        case timezone, status, createdAt, updatedAt, deletedAt, resourceData
    }
}
