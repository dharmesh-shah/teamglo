//
//  MyMatchesModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 14/09/21.
//

import Foundation

// MARK: - MyMatchesModel
struct MyMatchesModel: Codable {
    var status: String?
    var data: MatchesData?
    var message: String?
}

// MARK: - DataClass
struct MatchesData: Codable {
    var resources: [MyMatchesData]?
    var savedFilters: [GetUserFilterData]?

    enum CodingKeys: String, CodingKey {
        case resources
        case savedFilters = "saved_filters"
    }
}

// MARK: - MyMatchesData
struct MyMatchesData: Codable {
    var id: Int?
    var name: String?
    var logo: String?
    var price: Int?
    var level: String?
    var experience: Int?
    var intro: String?
    var projects: [Project]?
    var priority: Int?
    var responsibility, professionalSummary, title: String?
    var filterID: Int?
    var resourceTechnologies: [ResourceTechnology]?
    var highlight, isFeatured: Bool?

    enum CodingKeys: String, CodingKey {
        case id, name, price, level, experience, intro, projects, priority, responsibility
        case professionalSummary = "professional_summary"
        case title
        case filterID = "filter_id"
        case resourceTechnologies = "resource_technologies"
        case highlight
        case isFeatured = "is_featured"
    }
}
// MARK: - Project
struct Project: Codable {
    var title: String?
    var projectTechnology: [String]?
    var projectDescription: [String]?

}
// MARK: - ResourceTechnology
struct ResourceTechnology: Codable {
    var id, resourceID, technologyID: Int?
    @NullCodable var createdAt: String?
    @NullCodable var updatedAt: String?
    @NullCodable var deletedAt: String?
    var technology: Technology?

    enum CodingKeys: String, CodingKey {
        case id
        case resourceID = "resource_id"
        case technologyID = "technology_id"
        case createdAt, updatedAt, deletedAt, technology
    }
}
// MARK: - Technology
struct Technology: Codable {
    var name: String?
    var id: Int?

}
// MARK: - ScheduleInterview
struct ScheduleInterview: Codable {
    var id, meetingID: Int?
    var status: String?
    var resourceID: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case meetingID = "meeting_id"
        case status
        case resourceID = "resource_id"
    }
}
