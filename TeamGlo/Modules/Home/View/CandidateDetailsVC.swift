//
//  CandidateDetailsVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 09/09/21.
//

import UIKit

class CandidateDetailsVC: UIViewController,HomeStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var lblProfessionalSummary: UILabel!
    @IBOutlet weak var lblResposibility: UILabel!
    @IBOutlet weak var lblAboutCandidate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblTechnology: UILabel!
    @IBOutlet weak var lblCandidateName: UILabel!
    
    @IBOutlet weak var imgTechnology: UIImageView!
    @IBOutlet weak var collTechnologyType: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK: - Variable Declaration
    var objCandidateData : MyMatchesData?
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues()
    }
    
    //MARK: - Set Initial Values
    func setValues() {
        if let imgPath = objCandidateData?.logo {
            imgTechnology.setImgFullUrlWithNuke(imgPath)
        }
        lblCandidateName.text = objCandidateData?.name
        lblTechnology.text = objCandidateData?.intro
        lblExperience.text = "Experience : \(objCandidateData?.experience ?? 0) Years"
        lblAmount.text = "$\(objCandidateData?.price ?? 0)/-Year"
        lblAboutCandidate.text = objCandidateData?.intro
        lblResposibility.text = objCandidateData?.responsibility
        lblProfessionalSummary.text = objCandidateData?.professionalSummary

    }
    //MARK: - UIBUtton Action
    @IBAction func btnDownloadAction(_ sender: UIButton) {
        let view: BottomCustomAlertView = try! SwiftMessages.viewFromNib()
        view.configureDropShadow()
        view.lblMsg.text = "We have sent you N.G's (Node.JS Developer) \nCV on your Email Address\n\nKindly Check the same.\n\nThank you."
        view.lblTitle.isHidden = true
        view.btnCancel.isHidden = true
        view.mainButtonAction = { SwiftMessages.hide() }
        view.cancelButtonAction = { SwiftMessages.hide() }
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.duration = .forever
        config.presentationStyle = .bottom
        config.dimMode = .none
        SwiftMessages.show(config: config, view: view)
    }
}
//MARK: - PanModalPresentable Methods
extension CandidateDetailsVC: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }

     var panScrollable: UIScrollView? {
         return scrollView
     }
    var cornerRadius: CGFloat {
        return 40
    }
     var longFormHeight: PanModalHeight {
         return .contentHeight(scrollView.contentSize.height + 25)
     }
     var anchorModalToLongForm: Bool {
         return false
     }
    var showDragIndicator : Bool {
        return false
    }
}
//MARK: - UICollectionViewDelegate and UICollectionViewDataSource Methods
extension CandidateDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objCandidateData?.resourceTechnologies?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collTechnologyType.dequeueReusableCell(withReuseIdentifier: "TechnologyTypeCell", for: indexPath) as! TechnologyTypeCell
        cell.lblTechnologyType.text = objCandidateData?.resourceTechnologies?[indexPath.row].technology?.name
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 30)
    }
}
