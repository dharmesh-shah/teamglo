//
//  MyMatchesVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 09/09/21.
//

import UIKit

class MyMatchesVC: UIViewController ,HomeStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var tblCandidateList: UITableView!
    
    //MARK: - Variable Declaration
    var viewModel = CreateTeamViewModel()
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tblCandidateList.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tblCandidateList.w, height: 110))
        Global.shared.arrMyMatches.asDriver().drive (onNext: { [weak self] (_) in
            self?.tblCandidateList.reloadData()
        }).disposed(by: rx.disposeBag)
        getMyMatchesData()
    }
    
    //MARK: - UIButton Action
    @IBAction func btnScheduleInterviewAction(_ sender: UIButton) {
        let objScheduleInterviewVC = ScheduleInterviewVC.instantiate()
        presentPanModal(objScheduleInterviewVC)
    }
    
    //MARK: - API Call
    func getMyMatchesData() {
        viewModel.createFilterForTeam(view, [:]) { [weak self](error,arrUserResource) in
            if let error = error {
                self?.view.makeToast(error)
                return
            }
        }
    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension MyMatchesVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Global.shared.arrMyMatches.value.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCandidateList.dequeueReusableCell(withIdentifier: "CandidateTblCell", for: indexPath) as! CandidateTblCell
        let objMatchData = Global.shared.arrMyMatches.value[indexPath.row]
        if let imgLogoPath = objMatchData.logo {
                cell.imgTechnology.setImgFullUrlWithNuke(imgLogoPath)
        }
        cell.lblCandidateName.text = objMatchData.name
        cell.lblTechnology.text = objMatchData.title
        cell.lblExperience.text = "Experience : \(objMatchData.experience ?? 0) Years"
        cell.lblAmount.text = "$\(objMatchData.price ?? 0)/-Year"
        cell.arrTechnology = objMatchData.resourceTechnologies ?? []
        cell.collTechnologyType.reloadData()

        cell.btnReplace.action { [weak self] (btn) in
            let objReplaceCandidateVC = ReplaceCandidateVC.instantiate()
            self?.presentPanModal(objReplaceCandidateVC)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objCandidateDetailsVC = CandidateDetailsVC.instantiate()
        objCandidateDetailsVC.objCandidateData = Global.shared.arrMyMatches.value[indexPath.row]
        presentPanModal(objCandidateDetailsVC)

    }
}
