//
//  ReplaceCandidateVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 09/09/21.
//

import UIKit

class ReplaceCandidateVC: UIViewController, HomeStoryboarded {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblCandidateList: UITableView!
    
    //MARK: - Variable Declaration
    var viewModel = ReplaceCandidateViewModel()

    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        getReplaceResourceData()
    }
    
    func getReplaceResourceData() {
        var params = [[String:Any]]()
        var param = [String:Any]()
        for filter in viewModel.arrUserFilter {
            param[WebServiceParameter.experience] = filter.experience
            param[WebServiceParameter.technology] = filter.technology
            param[WebServiceParameter.noOfDev] = filter.noOfDev
            param[WebServiceParameter.exclude] = []
            param[WebServiceParameter.temp] = viewModel.arrMyMatches.map({$0.id})
            params.append(param)
        }
        
        viewModel.replaceResourceData(view, params) { [weak self](error) in
            if let error = error {
                self?.view.makeToast(error)
                return
            }
            self?.tblCandidateList.reloadData()
        }
    }

}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension ReplaceCandidateVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.arrMyMatches.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCandidateList.dequeueReusableCell(withIdentifier: "CandidateTblCell", for: indexPath) as! CandidateTblCell
        if let imgLogoPath = viewModel.arrMyMatches[indexPath.row].logo {
                cell.imgTechnology.setImgFullUrlWithNuke(imgLogoPath)
        }
        cell.lblCandidateName.text = viewModel.arrMyMatches[indexPath.row].name
        cell.lblTechnology.text = viewModel.arrMyMatches[indexPath.row].intro
        cell.lblExperience.text = "Experience : \(viewModel.arrMyMatches[indexPath.row].experience ?? 0) Years"
        cell.lblAmount.text = "$\(viewModel.arrMyMatches[indexPath.row].price ?? 0)/-Year"
        cell.arrTechnology = viewModel.arrMyMatches[indexPath.row].resourceTechnologies ?? []
        cell.collTechnologyType.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objCandidateDetailsVC = CandidateDetailsVC.instantiate()
        objCandidateDetailsVC.objCandidateData = viewModel.arrMyMatches[indexPath.row]
        presentPanModal(objCandidateDetailsVC)
    }
}
//MARK: - PanModalPresentable Methods
extension ReplaceCandidateVC: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }

     var panScrollable: UIScrollView? {
         return tblCandidateList
     }
    var cornerRadius: CGFloat {
        return 40
    }
     var longFormHeight: PanModalHeight {
         return .contentHeight(tblCandidateList.contentSize.height + 150)
     }
     var anchorModalToLongForm: Bool {
         return false
     }
    var showDragIndicator : Bool {
        return false
    }
}
