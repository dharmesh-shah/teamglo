//
//  ScheduleInterviewVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 09/09/21.
//

import UIKit

class ScheduleInterviewVC: UIViewController,HomeStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var txtSelectDate: UITextField!
    @IBOutlet weak var txtSelectTime: UITextField!
    @IBOutlet weak var txtTimeZone: UITextField!
    @IBOutlet weak var btnConfirm: UIButton!
    
    var pickerTimeZone = UIPickerView()
    let datePickerView = UIDatePicker()
    let timePickerView = UIDatePicker()
    let dateFormatter = DateFormatter()
    let timeFormatter = DateFormatter()
    var viewModel = ScheduleInterviewViewModel()
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialViews()
        createToolbar()
        setDateTimePicker()
    }
    
    //MARK: - Setup UI
    func setInitialViews() {
        pickerTimeZone.delegate = self
        pickerTimeZone.dataSource = self
        txtSelectDate.delegate = self
        txtSelectTime.delegate = self
        txtTimeZone.inputView = pickerTimeZone
    }
    func setDateTimePicker(){
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "yyyy/MM/dd"
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        if #available(iOS 13.4, *) {
            datePickerView.preferredDatePickerStyle = .compact
        } else {
        }
        self.txtSelectDate.inputView = datePickerView
        datePickerView.minimumDate = Date()
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
        
        
        timeFormatter.timeStyle = .medium
        timeFormatter.dateFormat = "HH:mm:ss"
        timePickerView.datePickerMode = UIDatePicker.Mode.time
        self.txtSelectTime.inputView = timePickerView
        if #available(iOS 13.4, *) {
            timePickerView.preferredDatePickerStyle = .compact
        } else {
        }
        timePickerView.addTarget(self, action: #selector(self.timePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        txtSelectDate.text = dateFormatter.string(from: sender.date)
    }
    @objc func timePickerValueChanged(sender:UIDatePicker) {
        txtSelectTime.text = timeFormatter.string(from: sender.date)
    }

    func createToolbar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()

        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissKeyboard))
        toolBar.setItems([spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        txtTimeZone.inputAccessoryView = toolBar

    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    //MARK: - UIButton Action
    @IBAction func btnCancelAction(_ sender: UIButton) {
        dismiss(animated: true)
    }
    @IBAction func btnConfirmAction(_ sender: UIButton) {
        if txtSelectDate.isEmpty {
            self.view.makeToast(AlertMessage.selectdate)
        } else if txtSelectTime.isEmpty {
            self.view.makeToast(AlertMessage.selectTime)
        } else if txtTimeZone.isEmpty {
            self.view.makeToast(AlertMessage.selectTimezone)
        } else {
            wsScheduleInterview()
        }
    }
    
    //MARK: - API Call
    func wsScheduleInterview () {
        viewModel.scheduleInterView(btnConfirm, arrResourceIds: Global.shared.arrMyMatches.value.map({($0.id ?? 0)}), strDateTime: "\(txtSelectDate.text ?? "") \(txtSelectTime.text ?? "")", strTimeZone: txtTimeZone.text ?? "") { [weak self] (error) in
            if let error = error {
                self?.view.makeToast(error)
                return
            }
            self?.dismiss(animated: true)
        }
    }
}
//MARK: - PanModalPresentable Methods
extension ScheduleInterviewVC: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }

     var panScrollable: UIScrollView? {
         return nil
     }
    var cornerRadius: CGFloat {
        return 40
    }
     var longFormHeight: PanModalHeight {
         guard let scrollView = panScrollable else { return .contentHeight(580) }
         // called once during presentation and stored
         scrollView.layoutIfNeeded()
         return .contentHeight(scrollView.contentSize.height)
     }
     var anchorModalToLongForm: Bool {
         return false
     }
    var showDragIndicator : Bool {
        return false
    }

}
extension ScheduleInterviewVC : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.arrTimeZone.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.arrTimeZone[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtTimeZone.text = viewModel.arrTimeZone[row]
    }
}
extension ScheduleInterviewVC : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            if textField == txtSelectDate {
                txtSelectDate.text = dateFormatter.string(from: datePickerView.date)
            } else if textField == txtSelectTime{
                txtSelectTime.text = timeFormatter.string(from: timePickerView.date)
            }
        }
    }
}
