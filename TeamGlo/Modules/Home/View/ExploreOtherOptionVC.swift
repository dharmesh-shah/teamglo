//
//  ExploreOtherOptionVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 13/09/21.
//

import UIKit

class ExploreOtherOptionVC: UIViewController, HomeStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var tblCandidateList: UITableView!

    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tblCandidateList.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tblCandidateList.w, height: 70))
    }
    //MARK: - UIButton Actions
    @IBAction func btnNotNowAction(_ sender: UIButton) {
    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension ExploreOtherOptionVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCandidateList.dequeueReusableCell(withIdentifier: "CandidateTblCell", for: indexPath) as! CandidateTblCell
        cell.collTechnologyType.reloadData()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 215
    }
}
//MARK: - PanModalPresentable Methods
extension ExploreOtherOptionVC: PanModalPresentable {

    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }

     var panScrollable: UIScrollView? {
         return tblCandidateList
     }
    var cornerRadius: CGFloat {
        return 40
    }
     var longFormHeight: PanModalHeight {
         return .contentHeight(tblCandidateList.contentSize.height)
     }
     var anchorModalToLongForm: Bool {
         return false
     }
    var showDragIndicator : Bool {
        return false
    }
}
