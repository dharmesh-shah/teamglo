//
//  HomeMasterVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 06/09/21.
//

import UIKit

class HomeMasterVC: UIViewController,HomeStoryboarded {
    
    //MARK: - IBOutlets
    @IBOutlet weak var homeTopSegment: ScrollableSegmentedControl!
    
    
    //MARK: - Variable Declarations
    var pageViewController: HomePageVC? {
        didSet {
            pageViewController?.objHomeMasterVC = self
        }
    }
    var selectedTopIndex = 0

    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setScrollableSegmentControl()
//        mainThread {
//            self.add(CreateTeamIntroVC.instantiate())
//        }
//        homeTopSegment.selectorType = .bottomBar
//        homeTopSegment.SelectedFont = UIFont(name: Font.interTextMedium, size: 15)!
//        homeTopSegment.normalFont = UIFont(name: Font.interTextRegular, size: 15)!

    }
    // MARK: - Setup ScrollableSegmentedControl
    fileprivate func setScrollableSegmentControl(){
        homeTopSegment.segmentStyle = .textOnly
        homeTopSegment.insertSegment(withTitle: "My Matches", image: nil, at: 0)
        homeTopSegment.insertSegment(withTitle: "My Interviews", image: nil, at: 1)
        homeTopSegment.insertSegment(withTitle: "Ready to Hire", image: nil, at: 2)
        homeTopSegment.underlineSelected = true
        
        homeTopSegment.selectedSegmentIndex = 0
        homeTopSegment.fixedSegmentWidth = false
        homeTopSegment.tintColor = .BlueTheme()
        homeTopSegment.selectedSegmentContentColor = .BlueTheme()
        homeTopSegment.underlineHeight = 3
//        objScrollableSegment.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.brandonTextRegular(size: 14)], for: .normal)
//        objScrollableSegment.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.brandonTextRegular(size: 15)], for: .selected)
        
        homeTopSegment.addTarget(self, action: #selector(self.segmentSelected(sender:)), for: .valueChanged)
        homeTopSegment.addTarget(self, action: #selector(self.segmentSelectedRepeat(sender:)), for: .touchDownRepeat)
        
    }
    @objc func segmentSelectedRepeat(sender:ScrollableSegmentedControl) {
    }
    @objc func segmentSelected(sender:ScrollableSegmentedControl) {
    }

    func pageViewIdexChange(_ index : Int) {
        homeTopSegment.selectedSegmentIndex = index
    }

    //MARK: - UIButton Actions

    @IBAction func homeTopSegmentAction(_ sender: ScrollableSegmentedControl) {
        pageViewController?.scrollToViewController(sender.selectedSegmentIndex)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let pageViewController = segue.destination as? HomePageVC {
            self.pageViewController = pageViewController
        }
    }
}
