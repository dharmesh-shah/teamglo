//
//  ReadyToHireVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 09/09/21.
//

import UIKit

class ReadyToHireVC: UIViewController ,HomeStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var tblCandidateList: ContentSizedTableView!
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tblCandidateList.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tblCandidateList.w, height: 30))
    }
    
    //MARK: - UIButton Actions
//    @IBAction func btnHireNowAction(_ sender: UIButton) {
//        let objSubscriptionVC = SubscriptionVC.instantiate()
//        self.navigationController?.pushViewController(objSubscriptionVC)
//    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension ReadyToHireVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCandidateList.dequeueReusableCell(withIdentifier: "InterviewTblCell", for: indexPath) as! InterviewTblCell
        cell.invalidateIntrinsicContentSize()
        cell.relaodCandidateList()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
