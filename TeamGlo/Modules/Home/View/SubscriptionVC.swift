//
//  SubscriptionVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 13/09/21.
//

import UIKit

class SubscriptionVC: UIViewController, HomeStoryboarded {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblCandidateList:
        ContentSizedTableView!
    
    @IBOutlet weak var txtMonthYear: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtCVC: UITextField!
    @IBOutlet weak var txtName: UITextField!
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton()
        setNavigationLogoWithBack()
        addNotificationProfile()
    }
    
    //MARK: - UIButton Actions
    @IBAction func btnPayAction(_ sender: UIButton) {
    }
}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension SubscriptionVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCandidateList.dequeueReusableCell(withIdentifier: "CandidateTblCell", for: indexPath) as! CandidateTblCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
