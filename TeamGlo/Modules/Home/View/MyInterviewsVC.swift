//
//  MyInterviewsVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 09/09/21.
//

import UIKit

class MyInterviewsVC: UIViewController ,HomeStoryboarded {

    @IBOutlet weak var tblInterviewList: UITableView!
    
    var viewModel = MyInterviewViewModel()
    
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        getMyInterviews()
        tblInterviewList.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tblInterviewList.w, height: 30))

    }
    
    func getMyInterviews() {
        viewModel.getMyInterviewsData(tblInterviewList) { [weak self] (error) in
            if let error = error {
                self?.view.makeToast(error)
                return
            }
            self?.tblInterviewList.reloadData()
        }
    }

}
//MARK: - UITableViewDelegate and UITableViewDataSource Methods
extension MyInterviewsVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5//viewModel.arrMyInterviewsData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblInterviewList.dequeueReusableCell(withIdentifier: "InterviewTblCell", for: indexPath) as! InterviewTblCell
//        cell.lblInterviewDateTime.text = viewModel.arrMyInterviewsData[indexPath.row].scheduledDateTime?.dateTime?.stringWithTimeZone(withFormat: "hh:mm a z, dd MMMM, yyyy",timeZone: viewModel.arrMyInterviewsData[indexPath.row].timezone ?? "")
//        cell.viewModel.arrMyMatches = viewModel.arrMyInterviewsData[indexPath.row].resourceData ?? []
        cell.invalidateIntrinsicContentSize()
        cell.relaodCandidateList()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
