//
//  ViewController.swift
//  TeamGlo
//
//  Created by Dharmesh on 02/09/21.
//

import UIKit

class WelcomeVC: UIViewController,MainStoryboarded {

    //MARK: - View Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //MARK: - UIButton Action
    @IBAction func btnHireDevelopersAction(_ sender: UIButton) {
        let objIntroductionVC = IntroductionVC.instantiate()
        self.navigationController?.pushViewController(objIntroductionVC)
    }
}

