//
//  LoginModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 06/09/21.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    var status: String?
    var data: LoginData?
    var message: String?
}

// MARK: - LoginData
struct LoginData: Codable {
    var id: Int?
    @NullCodable var name: String?
    @NullCodable var emailID: String?
    var userType: Int?
    var phoneNumber: String?
    @NullCodable var photo: String?
    var status: Bool?
    var stripeCustomerID: String?
    var accessToken, createdAt: String?
    @NullCodable var updatedAt: String?
    @NullCodable var deletedAt: String?
    var photoPath: String?
    var isScheduleInterview: Bool?
    var token: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case emailID = "email_id"
        case userType = "user_type"
        case phoneNumber = "phone_number"
        case photo, status
        case stripeCustomerID = "stripe_customer_id"
        case accessToken = "access_token"
        case createdAt, updatedAt, deletedAt
        case photoPath = "photo_path"
        case isScheduleInterview = "is_schedule_interview"
        case token
    }
}
