//
//  VerifyCodeVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 03/09/21.
//

import UIKit

class VerifyCodeVC: UIViewController,MainStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var otpView: DPOTPView!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    
    //MARK: - Variable Declaration
    weak var objLoginPageVC : LoginPageVC!
    var otpTimer: Timer?
    var counter = 30
    var viewModel = VerifyCodeViewModel() 

    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        mainThread {
            self.startTimer()
            self.btnResendOTP.isEnabled = false
        }
    }
    //MARK: - Timer Methods
    func startTimer() {
        otpTimer?.invalidate()
        otpTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerFire), userInfo: nil, repeats: true)
    }
    @objc func timerFire() {
        if counter > 0 {
            counter -= 1
            btnResendOTP.setTitle("Resend OTP in \(counter) Sec", for: .normal)
        } else {
            otpTimer?.invalidate()
            btnResendOTP.setTitle("Resend", for: .normal)
            btnResendOTP.isEnabled = true
            counter = 30
        }
    }

    //MARK: - UIButton Actions
    @IBAction func btnResendAction(_ sender: UIButton) {
        startTimer()
        btnResendOTP.isEnabled = false
    }
    @IBAction func btnVerifyAction(_ sender: UIButton) {
        if otpView.text?.count != 6 {
            self.view.makeToast(AlertMessage.invalidOTP)
        } else {
            viewModel.verifyOtp(btnVerify, strFirebaseVerificationID: objLoginPageVC.objPhoneNoVC.strVerificationID ?? "", otp: otpView.text ?? "123456") { [weak self](error) in
                self?.viewModel.verifyPhoneLogin(self?.btnVerify ?? UIButton(), phoneNumber: self?.objLoginPageVC.objPhoneNoVC.txtPhone.text?.components(separatedBy: CharacterSet(charactersIn: "() -")).joined() ?? "") {
                    [weak self] error,responseData  in
                    if let error = error {
                        self?.view.makeToast(error)
                        return
                    }
                    if responseData?.data?.emailID?.count == 0 {
                        self?.objLoginPageVC.scrollToViewController(2)
                    } else {
                        let objMainTabBarVC = MainTabBarVC.instantiate()
                        self?.navigationController?.pushViewController(objMainTabBarVC)
                        self?.navigationController?.navigationBar.isHidden = false
                    }
                }
            }
        }
    }
}
