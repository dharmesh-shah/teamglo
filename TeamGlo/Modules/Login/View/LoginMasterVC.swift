//
//  LoginMasterVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 03/09/21.
//

import UIKit

class LoginMasterVC: UIViewController, MainStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var lblStepCount: UILabel!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var viewThree: UIView!
    @IBOutlet weak var btnBack: UIButton!

    //MARK: - Variable Declarations
    var pageViewController: LoginPageVC? {
        didSet {
            pageViewController?.objLoginMasterVC = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK: - Function Methods
    func pageViewIdexChange(_ index : Int) {
        switch index {
        case 0: viewOne.backgroundColor = .BlueTheme()
            lblStepCount.text = "Step 1 Of 3"
            btnBack.isHidden = true
        case 1: viewTwo.backgroundColor = .BlueTheme()
            lblStepCount.text = "Step 2 Of 3"
            btnBack.isHidden = false
        case 2: viewThree.backgroundColor = .BlueTheme()
            lblStepCount.text = "Step 3 Of 3"
            btnBack.isHidden = false
         default:
            break
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let pageViewController = segue.destination as? LoginPageVC {
            self.pageViewController = pageViewController
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        switch pageViewController?.currentIndex {
        case 1 :
            viewThree.backgroundColor = .StepGrayColor()
            viewTwo.backgroundColor = .StepGrayColor()
            pageViewController?.scrollToViewController((pageViewController?.currentIndex ?? 0) - 1)
        case 2:
            viewThree.backgroundColor = .StepGrayColor()
            pageViewController?.scrollToViewController((pageViewController?.currentIndex ?? 0) - 1)
        default:
            break
        }
    }


}
