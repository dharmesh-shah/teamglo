//
//  PhoneNoVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 03/09/21.
//

import UIKit

class PhoneNoVC: UIViewController,MainStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var txtPhone: PhoneNumberTextField!

    //MARK: - Variable Declaration
    weak var objLoginPageVC : LoginPageVC!
    var strVerificationID : String?
    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        PhoneNumberKit.CountryCodePicker.commonCountryCodes = ["US", "CA", "IN", "AU", "GB", "DE", "MX"]
        PhoneNumberKit.CountryCodePicker.forceModalPresentation = true
        txtPhone.withPrefix = true
        txtPhone.withFlag = true
        txtPhone.withExamplePlaceholder = true
        txtPhone.withDefaultPickerUI = true
    }
    
    //MARK: - UIButton Actions
    @IBAction func btnVarifyAction(_ sender: UIButton) {
        if !txtPhone.isValidNumber {
            self.view.makeToast(AlertMessage.invalidPhoneNo)
        } else {
            Utility().showLoader()
            PhoneAuthProvider.provider().verifyPhoneNumber(txtPhone.text?.components(separatedBy: CharacterSet(charactersIn: "() -")).joined() ?? "", uiDelegate: nil) { [weak self] (verificationId, error) in
                Utility().hideLoader()
                guard let self = self else {return}
                if let error = error {
                    self.view.makeToast(error.localizedDescription)
                    return
                }
                self.strVerificationID = verificationId
                if self.strVerificationID != "" {
                    self.objLoginPageVC.scrollToViewController(1)
                }
            }
        }
    }

}
