//
//  LoginPageVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 03/09/21.
//

import UIKit

class LoginPageVC: UIPageViewController {

    // MARK: - Variable declaration
    weak var objLoginMasterVC : LoginMasterVC?
    var objPhoneNoVC : PhoneNoVC!
    var objVerifyCodeVC : VerifyCodeVC!
    var objLoginDetailsVC : LoginDetailsVC!
    var currentIndex = 0

    private(set) lazy var orderedViewControllers: [UIViewController] = {
        // The view controllers will be shown in this order
        
        objPhoneNoVC = PhoneNoVC.instantiate()
        objPhoneNoVC.objLoginPageVC = self

        objVerifyCodeVC = VerifyCodeVC.instantiate()
        objVerifyCodeVC.objLoginPageVC = self
        
        objLoginDetailsVC = LoginDetailsVC.instantiate()
        objLoginDetailsVC.objLoginPageVC = self
        
        return [objPhoneNoVC,
                objVerifyCodeVC,
                objLoginDetailsVC,
        ]
    }()

    //MARK: - View Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()

        if let initialViewController = orderedViewControllers.first {
            scrollToViewController(initialViewController)
        }
    }
    //MARK: - Other Methods
    func scrollToViewController(_ newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.firstIndex(of: firstViewController) {
            let direction: UIPageViewController.NavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            scrollToViewController(nextViewController, direction: direction)
        }
    }
    func scrollToViewController(_ viewController: UIViewController,
                                direction: UIPageViewController.NavigationDirection = .forward) {
        setViewControllers([viewController],
                           direction: direction,
                           animated: true,
                           completion: { (finished) -> Void in
                            // Setting the view controller programmatically does not fire
                            // any delegate methods, so we have to manually notify the
                            // 'tutorialDelegate' of the new index.
                            self.notifyTutorialDelegateOfNewIndex()
        })
    }
    
    
    private func notifyTutorialDelegateOfNewIndex() {
        
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.firstIndex(of: firstViewController) {
            currentIndex = index
            objLoginMasterVC?.pageViewIdexChange(index)
        }
    }
}
// MARK: - UIPageViewControllerDataSource & UIPageViewControllerDelegate
extension LoginPageVC : UIPageViewControllerDataSource , UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController)  else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        notifyTutorialDelegateOfNewIndex()
    }
    
}
