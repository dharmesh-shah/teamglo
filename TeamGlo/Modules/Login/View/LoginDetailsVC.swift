//
//  LoginDetailsVC.swift
//  TeamGlo
//
//  Created by Dharmesh on 03/09/21.
//

import UIKit

class LoginDetailsVC: UIViewController,MainStoryboarded {

    //MARK: - IBOutlets
    @IBOutlet weak var txtEmail: FloatLabelTextField!
    @IBOutlet weak var txtFullName: FloatLabelTextField!
    
    //MARK: - Variable Declaration
    weak var objLoginPageVC : LoginPageVC!
    var viewModel = LoginDetailsViewModel()

    //MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - UIButton Actions
    @IBAction func btnFinishAction(_ sender: UIButton) {
        if txtFullName.isEmpty {
            self.view.makeToast(AlertMessage.fullName)
        } else if txtEmail.isEmpty {
            self.view.makeToast(AlertMessage.email)
        } else if !txtEmail.validateEmail() {
            self.view.makeToast(AlertMessage.validEmail)
        } else {
            viewModel.userUpdate(view, name: txtFullName.text ?? "", email: txtEmail.text ?? "") {
                [weak self] error in
                if let error = error {
                    self?.view.makeToast(error)
                    return
                }
                let objMainTabBarVC = MainTabBarVC.instantiate()
                self?.navigationController?.pushViewController(objMainTabBarVC)
            }
        }
    }
    
}
