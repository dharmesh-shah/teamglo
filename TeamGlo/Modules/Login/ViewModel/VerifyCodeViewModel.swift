//
//  VerifyCodeViewModel.swift
//  TeamGlo
//
//  Created by datt on 06/09/21.
//

import UIKit

class VerifyCodeViewModel {
    
}

// MARK: Public Methods
extension VerifyCodeViewModel {
    
    func verifyPhoneLogin(_ loaderInView: UIView, phoneNumber: String, completion: @escaping (_ error: String?, _ response: LoginModel?) -> ()) {
        
        var params = [String:Any]()
        params[WebServiceParameter.phoneNumber] = phoneNumber
        params[WebServiceParameter.deviceId] = "Test"
        ApiCall().post(apiUrl: WebServiceURL.logIn, params: params, model: LoginModel.self, loaderInView: loaderInView) { result in
            switch result {
            
            case .success(let response):
                print(response)
                UserDefaults.standard.save(customObject: response.data, inKey: UserDefaultKey.loginData)
                completion(nil, response)
            case .failure(let failure):
                completion(failure.message, nil)
            case .error(let error):
                completion(error.debugDescription, nil)
            }
            
        }
    }
    func verifyOtp(_ loaderInView: UIView, strFirebaseVerificationID: String, otp : String, completion: @escaping (_ error: String?) -> ()) {
        Utility().showLoader()
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: strFirebaseVerificationID,
            verificationCode: otp)
        Auth.auth().signIn(with: credential) { [weak self] (authResult, error) in

            guard let self = self else {return}
            Utility().hideLoader()
            if let error = error {
                completion(error.localizedDescription)
                return
            }
            
            if authResult == nil { return }
            self.getUserIdToken(authResult) { (error,token) in
                if let error = error {
                    completion(error)
                    return
                }
            }
        }
    }
    private func getUserIdToken(_ authResult: AuthDataResult?,completion: @escaping (_ error: String?,_ token : String?) -> ()) {
        authResult?.user.getIDToken(completion: { (token, error) in
            Utility().hideLoader()
            if let error = error {
                completion(error.localizedDescription, nil)
                return
            }
            guard let token = token else {return}
            completion(nil, token)
        })
    }
}

// MARK: - Private Methods
private extension VerifyCodeViewModel {

}
