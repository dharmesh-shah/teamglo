//
//  LoginDetailsViewModel.swift
//  TeamGlo
//
//  Created by Dharmesh on 06/09/21.
//

import Foundation

class LoginDetailsViewModel {
    
}
extension LoginDetailsViewModel {
    func userUpdate(_ loaderInView: UIView, name: String, email: String ,completion: @escaping (_ error: String?) -> ()) {
        
        var params = [String:Any]()
        params[WebServiceParameter.name] = name
        params[WebServiceParameter.emailID] = email
        params[WebServiceParameter.id] = Global.shared.loginData()?.id
        ApiCall().post(apiUrl: WebServiceURL.update, params: params, model: LoginModel.self, loaderInView: loaderInView) { result in
            switch result {
            
            case .success(let response):
                print(response)
                UserDefaults.standard.save(customObject: response, inKey: UserDefaultKey.loginData)
                completion(nil)
            case .failure(let failure):
                completion(failure.message)
            case .error(let error):
                completion(error.debugDescription)
            }
            
        }
    }

}
