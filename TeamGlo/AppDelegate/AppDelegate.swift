//
//  AppDelegate.swift
//  TeamGlo
//
//  Created by Dharmesh on 02/09/21.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        setWindowRoot(window)
        return true
    }
    func setWindowRoot(_ window : UIWindow?)  {
        self.window = window
        if let loginData = UserDefaults.standard.retrieve(object: LoginData.self, fromKey: UserDefaultKey.loginData) {
            print("token----" + (loginData.token ?? ""))
            if (loginData.emailID != "") {
                let objMainTabBarVC = MainTabBarVC.instantiate()
                window?.rootViewController = SwipeNavigationController(rootViewController: objMainTabBarVC)
            } else {
                let objLoginMasterVC = LoginMasterVC.instantiate()
                window?.rootViewController = SwipeNavigationController(rootViewController: objLoginMasterVC)
                objLoginMasterVC.navigationController?.navigationBar.isHidden = true
                objLoginMasterVC.pageViewController?.scrollToViewController(2)
            }
        } else {
            let objWelcomeVC = WelcomeVC.instantiate()
            window?.rootViewController = SwipeNavigationController(rootViewController: objWelcomeVC)
            objWelcomeVC.navigationController?.navigationBar.isHidden = true
        }
        
        self.window?.makeKeyAndVisible()
        setSplashScreen()
    }
    func setSplashScreen() {
        let objSplashVC = SplashVC.instantiate()
        objSplashVC.modalPresentationStyle = .fullScreen
        self.window?.rootViewController?.present(objSplashVC , animated: false)
    }
}

