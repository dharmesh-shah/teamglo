//
//  StoryBoardExtension.swift
//  GymUser
//
//  Created by datt on 13/11/19.
//  Copyright © 2019 zaptech. All rights reserved.
//

import Foundation


protocol MainStoryboarded {
    static func instantiate() -> Self
}

extension MainStoryboarded where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
}
protocol HomeStoryboarded {
    static func instantiate() -> Self
}

extension HomeStoryboarded where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
}
protocol CreateStoryboarded {
    static func instantiate() -> Self
}

extension CreateStoryboarded where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Create", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
}
protocol SupportStoryboarded {
    static func instantiate() -> Self
}

extension SupportStoryboarded where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Support", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
}
protocol AppSettingStoryboarded {
    static func instantiate() -> Self
}

extension AppSettingStoryboarded where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "AppSetting", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
}
