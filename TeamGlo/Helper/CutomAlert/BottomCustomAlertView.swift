//
//  TacoDialogView.swift
//  Demo
//
//  Created by Tim Moose on 8/12/16.
//  Copyright © 2016 SwiftKick Mobile. All rights reserved.
//

import UIKit
import SwiftMessages

class BottomCustomAlertView: MessageView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    

    var mainButtonAction: (() -> Void)?
    var cancelButtonAction: (() -> Void)?
    
    @IBAction func btnMainAction(_ sender: UIButton) {
        mainButtonAction?()
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {
        cancelButtonAction?()
    }
    
}
