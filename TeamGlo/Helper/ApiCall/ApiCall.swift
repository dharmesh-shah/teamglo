//
//  ApiCall.swift
//  ApiCallWithDecodable
//
//  Created by datt on 12/06/18.
//  Copyright © 2018 datt. All rights reserved.
//

import UIKit

#if DEBUG
    let isDebug = true
#else
    let isDebug = false
#endif

enum ApiCallResult<Value,ResponseError : GeneralResponseModel> {
    case success(Value)
    case failure(ResponseError)
    case error(Error?)
}

class ApiCall: NSObject {
    
    let constValueField = "application/json"
    let constHeaderField = "Content-Type"
    
    var observationLoaderView: NSKeyValueObservation?
    
    func post<T : Decodable ,A>(apiUrl : String, params: [String: A], model: T.Type, isLoader : Bool = true , loaderInView : UIView? = nil, isErrorToast : Bool = false , completion: @escaping (ApiCallResult<T,GeneralResponseModel>) -> ()) {
        requestMethod(apiUrl: apiUrl, params: params as [String : AnyObject], method: "POST", model: model , isLoader : isLoader , loaderInView : loaderInView , isErrorToast : isErrorToast , completion: completion)
    }
    
    func put<T : Decodable ,A>(apiUrl : String, params: [String: A], model: T.Type, isLoader : Bool = true, isErrorToast : Bool = false , completion: @escaping (ApiCallResult<T,GeneralResponseModel>) -> ()) {
        requestMethod(apiUrl:apiUrl, params: params as [String : AnyObject], method: "PUT",model: model , isLoader : isLoader , isErrorToast : isErrorToast ,  completion: completion)
    }
    
    func get<T : Decodable>(apiUrl : String, model: T.Type , isLoader : Bool = true , loaderInView : UIView? = nil , isErrorToast : Bool = false , completion: @escaping (ApiCallResult<T,GeneralResponseModel>) -> ()) {
        requestMethod(apiUrl: apiUrl, params: [:], method: "GET", model: model , isLoader : isLoader , loaderInView : loaderInView , isErrorToast : isErrorToast , completion: completion)
    }
    
    func requestMethod<T : Decodable>(apiUrl : String, params: [String: Any], method: NSString, model: T.Type ,isLoader : Bool = true , loaderInView : UIView? = nil, isErrorToast : Bool = true , completion: @escaping (ApiCallResult<T,GeneralResponseModel>) -> ()) {
        
        guard case ConnectionCheck.isConnectedToNetwork() = true else {
            UIApplication.topViewController()?.view.makeToast(AlertMessage.networkConnection)
            let userInfo: [String : Any] =
                        [ NSLocalizedDescriptionKey :  NSLocalizedString("error", value: AlertMessage.networkConnection, comment: "") ,
                          NSLocalizedFailureReasonErrorKey : NSLocalizedString("error", value: AlertMessage.networkConnection, comment: "")]
            mainThread { completion(.error(NSError(domain: "TeamGlo", code: 502, userInfo: userInfo))) }
            return
        }
        var loaderView : UIView?
        if isLoader {
            if var view = loaderInView {
                self.addLoaderInView(&view, loaderView:&loaderView)
            } else {
                Utility().showLoader()
            }
        }
//        var params = params
        var request = URLRequest(url: URL(string: apiUrl)!)
        request.httpMethod = method as String
        request.setValue(constValueField, forHTTPHeaderField: constHeaderField)
        
        if let loginData = UserDefaults.standard.retrieve(object: LoginData.self, fromKey: UserDefaultKey.loginData) ,let strToken = loginData.accessToken {
            request.setValue(strToken, forHTTPHeaderField: "Authorization")
//            params[WebServiceParameter.userId] = loginData.id
        }
//        request.setValue("U2FsdGVkX19iakgq0n99D0wvaYtPtIjgE/9+2xu/Joc=", forHTTPHeaderField: "Authorization")
//        params[WebServiceParameter.userId] = 107
        if !params.isEmpty {
            let jsonTodo: Data
            do {
                jsonTodo = try JSONSerialization.data(withJSONObject: params, options: [])
//                if Global.isEncrypted {
//                    //                print(String(data: jsonTodo, encoding: .utf8)!)
//                    let jsonStr = AESHelper.encrypt(String(data: jsonTodo, encoding: .utf8) ?? "")
//                    let dic = ["data":jsonStr] as Dictionary<String,String>
//                    let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
//                    request.httpBody = jsonData
//                } else {
                    request.httpBody = jsonTodo
//                }
            } catch {
                print("Error: cannot create JSON from todo")
                return
            }
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task: URLSessionDataTask = session.dataTask(with : request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            delayWithSeconds(0.3) {
                if let loaderView = loaderView {
                    mainThread { loaderView.removeFromSuperview() }
                } else {
                    Utility().hideLoader()
                }
            }
    
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                //                Utility().hideLoader()
                return
            }
            
            
            let decoder = JSONDecoder()
            do {
                if isDebug , let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
                    print(convertedJsonIntoDict)
                }
                let dictResponse = try decoder.decode(GeneralResponseModel.self, from: data )

                let strStatus = dictResponse.status!

                if strStatus == "success" {
                   
//                    let dictResponsee = try decoder.decode(model, from: (Global.isEncrypted && !(model is GeneralResponseModel.Type)) ? self.decryptedData(dictResponse) : data )
                    let dictResponsee = try decoder.decode(model, from: data )
                    mainThread {
                        completion(.success(dictResponsee))
                    }
                }
                else {
                    if strStatus == "failed" {
                        mainThread {
                            Global.shared.logOut()
                        }
                    }
                    mainThread {
                        completion(.failure(dictResponse))
                        if isErrorToast {
                            UIApplication.topViewController()?.view.makeToast(dictResponse.message)
                
                        }
                    }
                }
                
                
            } catch let error as NSError {
                print(error.localizedDescription)

                mainThread {
                    completion(.error(error))
                }
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
                        print(convertedJsonIntoDict)
                        if ((convertedJsonIntoDict as? [String:Any])?["status"] as? Int) == 401 {
                            mainThread {
                                Global.shared.logOut()
                            }
                        }
                    }
                }  catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        })
        task.resume()
    }
    
//    func requestGetMethod<T : Decodable>(apiUrl : String, method: String, model: T.Type, isLoader : Bool = true, loaderInView : UIView? = nil , isErrorToast : Bool = false , completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()) {
//
//        guard case ConnectionCheck.isConnectedToNetwork() = true else {
//            UIApplication.topViewController()?.view.makeToast(AlertMessage.networkConnection)
//            mainThread { completion(false, AlertMessage.networkConnection as AnyObject) }
//            return
//        }
//
//        var loaderView : UIView?
//        if isLoader {
//            if var view = loaderInView {
//                self.addLoaderInView(&view, loaderView:&loaderView)
//            } else {
//                Utility().showLoader()
//            }
//        }
//
//        var request = URLRequest(url: URL(string: apiUrl)!)
//
//        request.httpMethod = method
//        //  request.addValue(constValueField, forHTTPHeaderField: constHeaderField)
//        if let loginData = UserDefaults.standard.retrieve(object: LoginData.self, fromKey: UserDefaultKey.loginData) ,let strToken = loginData.token { //}, method != "GET" {
//            request.setValue(strToken/*"U2FsdGVkX1+WTPqGrTwwRdHR676ck8F2RZVX0/JeZ7Q="*/, forHTTPHeaderField: "Authorization")
//        }
//
//        let session = URLSession(configuration: URLSessionConfiguration.default)
//        let task: URLSessionDataTask = session.dataTask(with : request as URLRequest, completionHandler: { (data, response, error) -> Void in
//            delayWithSeconds(0.3) {
//                if let loaderView = loaderView {
//                    mainThread { loaderView.removeFromSuperview() }
//                } else {
//                    Utility().hideLoader()
//                }
//            }
//
//
//            guard let data = data, error == nil else {
//                completion(false, nil)
//                return
//            }
//            let decoder = JSONDecoder()
//            do {
//                if isDebug , !Global.isEncrypted , let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
//                    print(convertedJsonIntoDict)
//                }
//                let dictResponse = try decoder.decode(GeneralResponseModel.self, from: data )
//
//                let strStatus = dictResponse.status!
//
//                if strStatus == 200 {
//
//                    let dictResponsee = try decoder.decode(model, from: (Global.isEncrypted && !(model is GeneralResponseModel.Type)) ? self.decryptedData(dictResponse) : data )
//                    mainThread {
//                        completion(true,dictResponsee as AnyObject)
//                    }
//                }
//                else{
//                    if strStatus == 401 && apiUrl != WebServiceURL.userLogout {
//                        mainThread {
//                            Global.shared.logOut()
//                        }
//                    }
//                    mainThread {
//                        completion(false, dictResponse.message as AnyObject)
//                        if isErrorToast {
//                            UIApplication.topViewController()?.view.makeToast(dictResponse.message)
//                        }
//                    }
//                }
//
//
//            } catch let error as NSError {
//                print(error.localizedDescription)
//                mainThread {
//                    completion(false, error as AnyObject)
//                }
//                do {
//                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
//                        print(convertedJsonIntoDict)
//                        if ((convertedJsonIntoDict as? [String:Any])?["status"] as? Int) == 401 && apiUrl != WebServiceURL.userLogout {
//                            mainThread {
//                                Global.shared.logOut()
//                            }
//                        }
//                    }
//                }  catch let error as NSError {
//                    print(error.localizedDescription)
//                }
//            }
//        })
//        task.resume()
//    }
//    func decryptedData(_ dictResponse: GeneralResponseModel) -> Data {
//        var jsonData = Data()
//        do {
//            let dataStr = AESHelper.decrypt(dictResponse.data ?? "").data(using: .utf8) ?? Data()
//            let jsonObj : Any
//            if dataStr.count != 0 {
//                jsonObj = try JSONSerialization.jsonObject(with: dataStr, options : .allowFragments)
//            } else {
//                jsonObj = ""
//            }
//            if isDebug {
//                print(["data":jsonObj ,"message":dictResponse.message ?? "", "status":dictResponse.status ?? ""])
//                print("Data ====> " + (String(data: dataStr, encoding: .utf8) ?? "") + "======")
//            }
//            if let jsonDic = jsonObj as? Dictionary<String,Any> {
//                let dic = ["data":jsonDic ,"message":dictResponse.message ?? "", "status":dictResponse.status ?? ""] as Dictionary<String,Any>
//                jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
//            } else if let jsonArray = jsonObj as? [Dictionary<String,Any>]  {
//                let dic = ["data":jsonArray ,"message":dictResponse.message ?? "", "status":dictResponse.status ?? ""] as Dictionary<String,Any>
//                jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
//            } else {
//                let dic = ["data":jsonObj ,"message":dictResponse.message ?? "", "status":dictResponse.status ?? ""] as Dictionary<String,Any>
//                jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//        return jsonData
//    }
    func addLoaderInView(_ view: inout UIView, loaderView : inout UIView?) {
        loaderView = UIView(frame: view.bounds)
        loaderView?.backgroundColor = getBGColor(view)
        let activityIndicator = UIActivityIndicatorView()
        if view.h < 100 || view.w < 100 {
            activityIndicator.style = .medium
        } else {
            activityIndicator.style = .large
        }
        
        activityIndicator.color = .lightGray
        activityIndicator.startAnimating()
        loaderView?.addSubview(activityIndicator)
        activityIndicator.anchorCenterSuperview()
        view.addSubview(loaderView!)
        observationLoaderView = view.observe(\UIView.bounds, options: .new) { [weak loaderView] view, change in
//            print("I'm now called \(view.bounds)")
            if let value =  change.newValue {
                loaderView?.frame = value
            }
        }
//        loaderView?.anchorCenterSuperview()
        //                loaderView?.fillToSuperview()
    }
    func getBGColor(_ view : UIView?) -> UIColor {
        guard let view = view , let bgColor = view.backgroundColor else {
            return .white
        }
        return bgColor == .clear ? getBGColor(view.superview) : (bgColor.rgba.alpha == 1 ? bgColor : bgColor.withAlphaComponent(1))
    }
}

class GeneralResponseModel : Decodable {
//    var data : String?
    var message : String?
    var status : String?
    
    enum CodingKeys: String, CodingKey {

           case status = "status"
//           case data = "data"
           case message = "message"
       }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
//        if Global.isEncrypted {
//            data = try values.decodeIfPresent(String.self, forKey: .data)
//        } else {
//            data = ""
//        }
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}

