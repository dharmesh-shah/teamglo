//
//  Utility.swift


//MARK: - Colors
extension UIColor {
    class func StepGrayColor() -> UIColor { return UIColor(r: 196, g: 196, b: 196)}
    class func BlueTheme() -> UIColor { return UIColor(named: "BlueTheme")!}
    class func GrayColor() -> UIColor { return UIColor(named: "GrayColor")!}
    class func BGColor() -> UIColor { return UIColor(named: "BGColor")!}
    class func PrimaryBlack() -> UIColor { return UIColor(named: "PrimaryBlack")!}

}

//MARK: - Font Name
enum Font {
    static let interTextBlack                   = "Inter-Black"
    static let interTextBold                    = "Inter-Bold"
    static let interTextExtraBold               = "Inter-ExtraBold"
    static let interTextExtraLight              = "Inter-ExtraLight"
    static let interTextLight                   = "Inter-Light"
    static let interTextMedium                  = "Inter-Medium"
    static let interTextRegular                 = "Inter-Regular"
    static let interTextSemiBold                = "Inter-SemiBold"
    static let interTextThin                    = "Inter-Thin"
}
extension UIFont {
    class func interTextRegular(size: CGFloat) -> UIFont { return UIFont(name: Font.interTextRegular, size: size) ?? UIFont.systemFont(ofSize: size) }
    class func interTextMedium(size: CGFloat) -> UIFont { return UIFont(name: Font.interTextMedium, size: size) ?? UIFont.systemFont(ofSize: size) }
    class func interTextBold(size: CGFloat) -> UIFont { return UIFont(name: Font.interTextBold, size: size) ?? UIFont.systemFont(ofSize: size) }
    class func interTextSemiBold(size: CGFloat) -> UIFont { return UIFont(name: Font.interTextSemiBold, size: size) ?? UIFont.systemFont(ofSize: size) }

}
//MARK: - Title
enum Title {
    
    static let builderfly = "Builderfly"
    static let setting = "Setting"
    static let products = "Products"
    static let orders = "Orders"
    static let customers = "Customers"
    static let myAccount = "My Account"
    static let orderDetail = "Order Detail"
    static let orderStatus = "Order Status"
    static let paymentStatus = "Payment Status"
    static let addCustomers = "Add Customers"
    static let addCustomerAddress = "Add Customer Address"
    static let customerGroup = "Customers Group"
    static let addCustomerGroups = "Add Customers Group"
    static let shipments = "Shipments"
    static let shipmentDetail = "Shipment Detail"
    static let addProduct = "Add Product"
    static let productDetail = "Product Detail"
    static let productDescription = "Product Description"
    static let assignCategory = "Assign Category"
    static let SEO = "SEO"
    static let stockControl = "Stock Control"
    static let relatedProducts = "Related Products"
    static let variants = "Variants"
    static let bulkPricing = "Bulk Pricing"
    static let category = "Category"
    static let advanced = "Advanced"
    static let productNameSKU = "Product Name & SKU"
    static let subCategory = "Sub Category"
    static let editCategory = "Edit Category"
    static let addCategory = "Add Category"
    static let variantsDetail = "Variants Detail"
    static let variantsColorCode = "Variants Color Code"
    static let variantsSizeCode = "Variants Size Code"
    static let website = "Website"
    static let mobileApps = "Mobile Apps"
    static let emailCommunication = "Email Communication"
    static let storeCurrency = "Store Currency"
    static let businessAddress = "Business Address"
    static let changePassword = "Change Password"
    static let myProfile = "My Profile"

}


//MARK: - Message's
enum AlertMessage {
    
    //In Progress Message
    static let inProgress = "In Progress"
    
    //Internet Connection Message
    static let networkConnection = "You are not connected to internet. Please connect and try again"
    
    //Contact Us Message
    static let contactUs = "Your query has been submitted successfully"
    
    //Camera, Images and ALbums Related Messages
    static let cameraPermission = "Please enable camera access from Privacy Settings"
    static let photoLibraryPermission = "Please enable access for photos from Privacy Settings"
    static let noCamera = "Device Has No Camera"
    
    //Validation Messages
    static let firstName = "Please enter first name"
    static let validFName = "First name must contain atleast 3 characters and maximum 25 characters"
    static let lastName = "Please enter last name"
    static let validLName = "Last name must contain atleast 3 characters and maximum 25 characters"
    static let validEmail = "Please enter valid email"
    static let companyName = "Please enter company"
    static let email = "Please enter email"
    static let password = "Please enter password"
    static let passwordCharacter = "Password must contain atleast 8 characters and maximum 15 characters"
    static let validPassword = "Password should contain atleast one uppercase letter, one lowercase letter, one digit and one special character with minimum eight character length."
    static let spacePassword = "Please remove whitespace from password"
    static let phone = "Please enter phone number"
    static let phoneCharacter = "Phone number must contain atleast 8 digits and maximum 15 digits"
    static let fax = "Please enter fax number"
    static let faxCharacter = "Fax number must contain atleast 5 digits and maximum 15 digits"
    static let addressOne = "Please enter street address one"
    static let addressTwo = "Please enter street address two"
    static let city = "Please enter city"
    static let state = "Please select state"
    static let zipCode = "Please enter zip code"
    static let country = "Please select country"
    static let oldPassword = "Old  password required"
    static let newPassword = "New password required"
    static let confirmPassword = "Confirm password require"
    static let passwordNotMatch = "Confirm password not match with new password"
    static let msgLogout = "Do you want to logout?"
    static let msgConfirm = "Please Confirm!"
    static let removeWishlist = "Are you sure, you want to remove this item from your wishlist?"
    static let removeAddress = "Are you sure, you want to remove this address?"
    static let removeCart = "Are you sure, you want to remove this item from your cart?"
    static let review = "Please enter Review"
    static let reviewHeading = "Please enter Review Heading"
    static let invalidPhoneNo = "Please enter valid phone number"
    static let invalidOTP = "Please enter valid OTP"
    static let fullName = "Please enter full name"
    static let selectTechnology = "Please select technology"
    static let selectExperience = "Please select experience"
    static let selectNoOfPosition = "Please select no of position"
    static let selectdate = "Please select date"
    static let selectTime = "Please select time"
    static let selectTimezone = "Please select timezone"

    //Webservice Fail Message
    static let error = "Something went wrong. Please try after sometime"
    
    //Camera, Images and ALbums Related Messages
    static let msgPhotoLibraryPermission = "Please enable access for photos from Privacy Settings"
    static let msgCameraPermission = "Please enable camera access from Privacy Settings"
    static let msgNoCamera = "Device Has No Camera"
    
    //iTune Store Link not available Message
    static let msgiTuneStoreLink = "App is not Live yet. Sorry, you cannot Share App now. Please come after sometime."
}

//MARK: - Web Service URLs
enum WebServiceURL {
    static let mainURL = { () -> String in
        if UserDefaults.standard.bool(forKey: UserDefaultKey.isTesting) {
//            return "https://hire.zaptechsolutions.com:4500/" // live Testing
//            return "https://dev.teamglo.io:4700/"
            return "http://172.16.0.176:4500/" // Nikhil Local
        } else {
//            return "https://hire.zaptechsolutions.com:4500/" // live Build
//            return "https://dev.teamglo.io:4700/"
            return "http://172.16.0.176:4500/" // Nikhil Local
        }
    }()

//    static let mainURL = "http://3.6.75.52:5200/" // live Build
    static let apiURL = mainURL + "api/"
    static let imageURL = mainURL + "assets/uploads/"
    static let thumbURL = mainURL + "uploads/thumb/"
    static let serverImageURL = mainURL + "app/"
    static let logIn = apiURL + "login"
    static let update = apiURL + "update"
    static let searchResources = apiURL + "resource/search-new"
    static let scheduleInterview = apiURL + "schedule-interview"
    static let technologyList = apiURL + "technology/list"
    static let experienceList = apiURL + "experience/list"
    static let userGetFilters = apiURL + "user/get-filters-new"
    static let meetings = apiURL + "meetings"
    static let replaceResource = apiURL + "user/replace-resource"

}

//MARK: - Web Service Parameters
enum WebServiceParameter {
    static let id = "id"
    static let emailID = "email_id"
    static let password = "password"
    static let phoneNumber = "phone_number"
    static let otp = "otp"
    static let deviceId = "device_id"
    static let name = "name"
    static let userId = "user_id"
    static let filters = "filters"
    static let filterId = "filter_id"
    static let resourceId = "resource_id"
    static let scheduledDateTime = "scheduled_date_time"
    static let timezone = "timezone"
    static let experience = "experience"
    static let technology = "technology"
    static let noOfDev = "no_of_dev"
    static let temp = "temp"
    static let exclude = "exclude"

}
//MARK: -  NotificationCenter
enum AppSocketNotificationCenter {
    // Trending Video
    static let newTrendVideo = Notification.Name("new-trend-video")
    // community
    static let removeAnswerComment = Notification.Name("remove-answer-comment")
    static let newAnswerComment = Notification.Name("new-answer-comment")
    static let newQuestion = Notification.Name("new-question")
    static let newAnswer = Notification.Name("new-answer")


}
enum AppNotificationCenter {
    static let selectedAddressChanged = Notification.Name("selectedAddressChanged")
    static let rateMeCommentCountUpdate      =  Notification.Name("rateMeCommentCountUpdate")
    static let friendListChanged      =  Notification.Name("friendListChanged")
}


//MARK: - Framework Keys
enum GlobalKeys {
//    static let razorPayKey = "rzp_test_FB2C9kMrB5fmeD"
    static let razorPayKey = "rzp_test_PKZRt4NGyfabME"
    static let googlePlaceAndMapKey = "AIzaSyBsKueh0ngfu7FcMyJ_JN02hgxCOkRUHng"
    static let googleGeoCodeKey = "AIzaSyDWC3PD5PRAlyrRIfGu0xZ0YTxyPf5vq0I"
}
//MARK: - UserDefault
enum UserDefaultKey {
    static let loginData = "LoginData"
    static let deviceToken = "deviceToken"
    static let gender = "gender"
    static let isTesting = "isTesting"
    static let savedFilters = "savedFilters"
}

//MARK: - Constants
struct Constants {
    
    //MARK: - Global Utility
    static let appName    = Bundle.main.infoDictionary!["CFBundleName"] as! String
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let pageLimit = 50
    static var keyWindow : UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }

    //MARK: - Device Type
    enum UIUserInterfaceIdiom : Int{
        case Unspecified
        case Phone
        case Pad
    }
    
    //MARK: - Screen Size
    struct ScreenSize {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    //MARK: - DateFormat
    struct DateFormat {
        static let date = "dd/MM/yyyy"
        static let time = "hh:mm a"
    }
}

//MARK: - DateTime Format
enum DateAndTimeFormatString {
    static let strDateFormat_yyyymmddhhmmss = "yyyy-MM-dd HH:mm:ss"
    static let strDateFormat_MMMddyyyhhmmsssa = "MMM dd, yyyy hh:mm:ss a"
    static let strDateFormat_ddMMM = "dd MMM"
    static let strDateFormat_ddMMMYYYY = "dd-MMM-YYYY"
}
