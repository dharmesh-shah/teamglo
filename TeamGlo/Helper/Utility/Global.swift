//
//  Global.swift
//  Theme1Template
//
//  Created by dharmesh on 07/09/18.
//  Copyright © 2018 com.zaptechsolutions. All rights reserved.
//



class Global {
    static let shared = Global()

    var isGoogleSignin = false
    weak var objMainTabBarVC : MainTabBarVC?
    var arrMyMatches : BehaviorRelay<[MyMatchesData]> = .init(value: [])
    var arrUserFilter : BehaviorRelay<[GetUserFilterData]> = .init(value: [])

    func loginData() -> LoginData? {
        if let loginData = UserDefaults.standard.retrieve(object: LoginData.self, fromKey: UserDefaultKey.loginData)  {
            return loginData
        }
        return nil
    }
//
//
//    func wsUploadFile(_ filePaths : [String] , completion: @escaping (FileUploadModel)->()) {
//        var filesKey = ""
//        if filePaths.count == 1 { filesKey = "files" }
//        else { filesKey = "files[]" }
//
//        let arrFiles = [
//            [multiPartFieldName: filesKey,/*(if only 1 file upload don't use [])*/
//                multiPartPathURLs: filePaths]
//        ]
//
//        MultiPart().callPostWebService(WebServiceURL.fileUpload, parameters: nil, filePathArr: arrFiles, model: FileUploadModel.self) { (success, responseData) in
//            if success, let responseData = responseData as? FileUploadModel {
//                completion(responseData)
//            }
//        }
//    }
//
    func logOut() {
//        UserDefaults.standard.removeObject(forKey: UserDefaultKey.loginData)
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
//        appDelegate.setWindowRoot(appDelegate.window)
//        ApiCall().get(apiUrl: WebServiceURL.userLogout, model: GeneralResponseModel.self) {
//            (success, response) in
//        }
//        SocketIOManager.shared.closeConnection()
//        SocketIOManager.shared.socket = nil
//
//        //BuilderFly Logout
//        FitShopUtility().logOutMethod()
    }

    
}
